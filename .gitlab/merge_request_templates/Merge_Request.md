<!--

Have you read the Code of Conduct?
By filing a Merge Request, you're expected to comply with it, including treating everyone with respect.
Read CONTRIBUTING before submitting a merge request.

-->

# Description of the Change
(We must be able to understand the design of your change from this description.
If we can't get a good idea of what the code will be doing from the description here, the merge request may be closed at the maintainers' discretion.
Keep in mind that the maintainer reviewing this MR may not be familiar with or have worked with the code here recently, so walk us through the concepts.)

## Alternate Designs
(Explain what other alternates were considered and why the proposed version was selected)

## Why should this be in the software?
(Explain why this functionality should be in PiFly)

### Benefits
(What benefits will be realized by the code change?)

### Possible drawbacks
(What are the possible side effects or negative impacts of the code change?)

## Additional Information
(Any additional information, configuration or data that might be necessary to reproduce the issue.)

# Testing instructions
(Describe how this merge request can be tested by a third person.)

# Related issues and merge requests

## Issues
+ issue 0
+ issue 1

## Merge requests
+ mr0

**BY REMOVING THIS LINE, I ATTEST THAT I'VE FOLLOWED THE TEMPLATE AND PROVIDED A TITLE, as well as searched for other possible duplicate merge requests.
I acknowledge if this line isn't removed or the template isn't respected, my merge request will be closed without warning**