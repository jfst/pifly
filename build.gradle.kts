////////////////////////////////////////////////////////////////////////////////
// PiFly                                                                       /
// Copyright (C) 2021-2024 PiFly Team                                          /
//                                                                             /
// This file is part of PiFly.                                                 /
//                                                                             /
// PiFly is free software: you can redistribute it and/or modify               /
// it under the terms of the GNU General Public License as published by        /
// the Free Software Foundation, either version 3 of the License, or           /
// (at your option) any later version.                                         /
//                                                                             /
// PiFly is distributed in the hope that it will be useful,                    /
// but WITHOUT ANY WARRANTY; without even the implied warranty of              /
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               /
// GNU General Public License for more details.                                /
//                                                                             /
// You should have received a copy of the GNU General Public License           /
// along with PiFly. If not, see <http://www.gnu.org/licenses/>.               /
////////////////////////////////////////////////////////////////////////////////

plugins {
  id("java")
  id("application")
  id("idea")
  id("jacoco")
  id("checkstyle")
  id("maven-publish")
  id("org.openjfx.javafxplugin").version("0.1.0") // https://plugins.gradle.org/plugin/org.openjfx.javafxplugin
  id("io.spring.dependency-management").version("1.1.4") // https://plugins.gradle.org/plugin/io.spring.dependency-management
  id("org.springframework.boot").version("3.2.4") // https://plugins.gradle.org/plugin/org.springframework.boot
  id("org.cyclonedx.bom") version "1.8.2" // https://github.com/CycloneDX/cyclonedx-gradle-plugin
}

repositories {
  mavenCentral()
  maven("https://gitlab.com/api/v4/projects/48142946/packages/maven")
  maven("https://gitlab.com/api/v4/projects/48143613/packages/maven")
  maven("https://gitlab.com/api/v4/projects/48142974/packages/maven")
}

dependencies {
  implementation("org.springframework.boot:spring-boot-starter")
  implementation("org.springframework.boot:spring-boot-starter-webflux")
  compileOnly("org.projectlombok:lombok")
  annotationProcessor("org.projectlombok:lombok")
  testImplementation("org.springframework.boot:spring-boot-starter-test")

  implementation("com.fasterxml.jackson.dataformat:jackson-dataformat-xml")

  // https://github.com/Kotlin/kotlinx.serialization
  implementation("org.jetbrains.kotlinx:kotlinx-serialization-core-jvm:1.6.3")

  // https://search.maven.org/artifact/org.controlsfx/controlsfx
  implementation("org.controlsfx:controlsfx:11.2.1")

  // https://github.com/dukke/Transit
  implementation("com.pixelduke:transit:1.0.0")

  // https://github.com/sothawo/mapjfx
  // https://mvnrepository.com/artifact/com.sothawo/mapjfx
  implementation("com.sothawo:mapjfx:3.1.0")

  // https://github.com/JetBrains/java-annotations
  // https://mvnrepository.com/artifact/org.jetbrains/annotations
  implementation("org.jetbrains:annotations:24.1.0")

  // https://mvnrepository.com/artifact/io.github.mivek/metarParser
  implementation("io.github.mivek:metarParser:2.15.2")
  implementation("io.github.mivek:metarParser-services:2.15.2")
  implementation("io.github.mivek:metarParser-entities:2.15.2")
  implementation("io.github.mivek:metarParser-commons:2.15.2")
  implementation("io.github.mivek:metarParser-parsers:2.15.2")
  implementation("io.github.mivek:metarParser-spi:2.15.2")

  // https://gitlab.com/jfst/jflightplan
  // https://gitlab.com/jfst/jflightplan/-/packages
  implementation("de.greluc.kfst:kfp:4.0.0")
  //implementation(files("libs/jflightplan-3.3.0.jar"))

  // https://gitlab.com/jfst/jsimconnect
  // https://gitlab.com/jfst/jsimconnect/-/packages
  implementation("de.greluc.jfst:jsc:0.9.4")
  //implementation(files("libs/jsimconnect-0.9.3.jar"))
}

base {
  group = "de.greluc.jfst"
  version = "1.2.1"
  description = "PiFly shows different flight simulator related views."
}

configurations {
  compileOnly {
    extendsFrom(configurations.annotationProcessor.get())
  }
}

java {
  sourceCompatibility = JavaVersion.VERSION_21
}

javafx {
    application {
        mainClass.set("de.greluc.jfst.pifly.PiFly")
    }

    springBoot {
        mainClass.set("de.greluc.jfst.pifly.PiFly")
    }
  modules("javafx.controls", "javafx.fxml", "javafx.web")
  version = "21"
}

idea {
  module {
    inheritOutputDirs = true
  }
}

checkstyle {
  toolVersion = "10.14.2" // https://github.com/checkstyle/checkstyle
}

tasks.test {
  useJUnitPlatform()
  finalizedBy(tasks.jacocoTestReport) // report is always generated after tests run
}

tasks.jacocoTestReport {
  dependsOn(tasks.test) // tests are required to run before generating the report
  reports {
    xml.required.set(true)
    csv.required.set(true)
    html.required.set(true)
  }
}

configure<PublishingExtension> {
  publications {
    create<MavenPublication>("bootJava") {
      artifact(tasks.named("bootJar"))
    }
  }
  repositories {
    maven {
      url = uri("https://gitlab.com/api/v4/projects/47953583/packages/maven")
      name = "GitLab"
      credentials(HttpHeaderCredentials::class) {
        name = "Job-Token"
        value = System.getenv("CI_JOB_TOKEN")
      }
      authentication {
        create("header", HttpHeaderAuthentication::class)
      }
    }
  }
}