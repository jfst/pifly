////////////////////////////////////////////////////////////////////////////////
// PiFly                                                                       /
// Copyright (C) 2021-2024 PiFly Team                                          /
//                                                                             /
// This file is part of PiFly.                                                 /
//                                                                             /
// PiFly is free software: you can redistribute it and/or modify               /
// it under the terms of the GNU General Public License as published by        /
// the Free Software Foundation, either version 3 of the License, or           /
// (at your option) any later version.                                         /
//                                                                             /
// PiFly is distributed in the hope that it will be useful,                    /
// but WITHOUT ANY WARRANTY; without even the implied warranty of              /
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               /
// GNU General Public License for more details.                                /
//                                                                             /
// You should have received a copy of the GNU General Public License           /
// along with PiFly. If not, see <http://www.gnu.org/licenses/>.               /
////////////////////////////////////////////////////////////////////////////////

package de.greluc.jfst.pifly;

import lombok.Generated;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NonNls;

/**
 * @author Lucas Greuloch (greluc, lucas.greuloch@protonmail.com)
 * @since 1.0.0
 * @version 1.2.1
 */
@SuppressWarnings("unused")
public class CommonConstants {

  public static final String SETTINGS_FILE_NAME = "application.properties";
  // miscellaneous
  public static final String DELIMITER = "----------------------------------------";
  public static final String UTILITY_CLASS = "Utility class";
  // log messages
  @NonNls
  public static final String CLOSE_SIMCONNECT = "Exception while trying to close simconnect";

  /**
   * Used to exclude the unused constructor from code coverage evaluation.
   */
  @Contract(value = " -> fail", pure = true)
  @Generated
  private CommonConstants() {
    throw new IllegalStateException(CommonConstants.UTILITY_CLASS);
  }
}
