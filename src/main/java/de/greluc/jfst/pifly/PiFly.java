////////////////////////////////////////////////////////////////////////////////
// PiFly                                                                       /
// Copyright (C) 2021-2024 PiFly Team                                          /
//                                                                             /
// This file is part of PiFly.                                                 /
//                                                                             /
// PiFly is free software: you can redistribute it and/or modify               /
// it under the terms of the GNU General Public License as published by        /
// the Free Software Foundation, either version 3 of the License, or           /
// (at your option) any later version.                                         /
//                                                                             /
// PiFly is distributed in the hope that it will be useful,                    /
// but WITHOUT ANY WARRANTY; without even the implied warranty of              /
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               /
// GNU General Public License for more details.                                /
//                                                                             /
// You should have received a copy of the GNU General Public License           /
// along with PiFly. If not, see <http://www.gnu.org/licenses/>.               /
////////////////////////////////////////////////////////////////////////////////

package de.greluc.jfst.pifly;

import de.greluc.kfst.kfp.OFPConfiguration;
import javafx.application.Application;
import lombok.Generated;
import org.jetbrains.annotations.Contract;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

/**
 * This class starts the {@code ClientApplication}. It is necessary to run this JavaFX application
 * as a FatJAR/UberJAR with Java 17+.
 *
 * @author Lucas Greuloch (greluc, lucas.greuloch@protonmail.com)
 * @since 1.0.0
 * @version 1.2.1
 */
@SpringBootApplication
@Import(OFPConfiguration.class)
public class PiFly {

  /**
   * Constructs a new {@code PiFly} instance.
   */
  @Contract(pure = true)
  @Generated
  public PiFly() {
    super();
  }

  /**
   * Entry point to start the PiFly application.
   *
   * @param args Command line arguments.
   */
  @Generated
  public static void main(String[] args) {
    Application.launch(ClientApplication.class, args);
  }
}