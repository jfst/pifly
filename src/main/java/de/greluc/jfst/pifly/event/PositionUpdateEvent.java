////////////////////////////////////////////////////////////////////////////////
// PiFly                                                                       /
// Copyright (C) 2021-2024 PiFly Team                                          /
//                                                                             /
// This file is part of PiFly.                                                 /
//                                                                             /
// PiFly is free software: you can redistribute it and/or modify               /
// it under the terms of the GNU General Public License as published by        /
// the Free Software Foundation, either version 3 of the License, or           /
// (at your option) any later version.                                         /
//                                                                             /
// PiFly is distributed in the hope that it will be useful,                    /
// but WITHOUT ANY WARRANTY; without even the implied warranty of              /
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               /
// GNU General Public License for more details.                                /
//                                                                             /
// You should have received a copy of the GNU General Public License           /
// along with PiFly. If not, see <http://www.gnu.org/licenses/>.               /
////////////////////////////////////////////////////////////////////////////////

package de.greluc.jfst.pifly.event;

import de.greluc.jfst.pifly.service.SimConnectPositionService;
import lombok.EqualsAndHashCode;
import lombok.Generated;
import lombok.Getter;
import lombok.NonNull;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.ApplicationEvent;

/**
 * Event used to publish a new position of the player a/c.
 * <p>
 * Published by the {@link SimConnectPositionService}.
 *
 * @author Lucas Greuloch (greluc, lucas.greuloch@protonmail.com)
 * @since 1.0.0
 * @version 1.2.1
 */
@EqualsAndHashCode(callSuper = false)
public class PositionUpdateEvent extends ApplicationEvent {

  @Getter
  private final double latitude;
  @Getter
  private final double longitude;
  @Getter
  private final long altitude;

  /**
   * Used to set the necessary data of this event.
   *
   * @param source {@link Object} that published this event.
   */
  @Generated
  public PositionUpdateEvent(@NotNull @NonNull Object source, double latitude,
      double longitude, long altitude) {
    super(source);
    this.latitude = latitude;
    this.longitude = longitude;
    this.altitude = altitude;
  }
}
