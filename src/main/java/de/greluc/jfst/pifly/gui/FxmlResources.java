////////////////////////////////////////////////////////////////////////////////
// PiFly                                                                       /
// Copyright (C) 2021-2024 PiFly Team                                          /
//                                                                             /
// This file is part of PiFly.                                                 /
//                                                                             /
// PiFly is free software: you can redistribute it and/or modify               /
// it under the terms of the GNU General Public License as published by        /
// the Free Software Foundation, either version 3 of the License, or           /
// (at your option) any later version.                                         /
//                                                                             /
// PiFly is distributed in the hope that it will be useful,                    /
// but WITHOUT ANY WARRANTY; without even the implied warranty of              /
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               /
// GNU General Public License for more details.                                /
//                                                                             /
// You should have received a copy of the GNU General Public License           /
// along with PiFly. If not, see <http://www.gnu.org/licenses/>.               /
////////////////////////////////////////////////////////////////////////////////

package de.greluc.jfst.pifly.gui;

import lombok.Generated;
import lombok.Getter;
import org.jetbrains.annotations.Contract;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

/**
 * Contains the paths to the {@code FXML} files ands builds {@link Resource}s for them.
 *
 * @author Lucas Greuloch (greluc, lucas.greuloch@protonmail.com)
 * @since 1.0.0
 * @version 1.2.1
 */
@Component
public class FxmlResources {

  public static final String CLASSPATH = "classpath:";
  public static final String CLASSPATH_FXML = CLASSPATH + "/fxml/";
  public static final String CLASSPATH_FXML_MAIN = CLASSPATH_FXML + "MainView.fxml";
  public static final String CLASSPATH_FXML_SELECTION = CLASSPATH_FXML + "SelectionView.fxml";
  public static final String CLASSPATH_FXML_WELCOME = CLASSPATH_FXML + "WelcomeView.fxml";
  public static final String CLASSPATH_FXML_SETTINGS = CLASSPATH_FXML + "SettingsView.fxml";
  public static final String CLASSPATH_FXML_METAR = CLASSPATH_FXML + "MetarView.fxml";
  public static final String CLASSPATH_FXML_TAF = CLASSPATH_FXML + "TafView.fxml";
  public static final String CLASSPATH_FXML_TAF_TAB = CLASSPATH_FXML + "TafTabView.fxml";
  public static final String CLASSPATH_FXML_ICAO = CLASSPATH_FXML + "IcaoView.fxml";
  public static final String CLASSPATH_FXML_OFP = CLASSPATH_FXML + "OfpView.fxml";
  public static final String CLASSPATH_FXML_OFP_TAB = CLASSPATH_FXML + "OfpTAbView.fxml";
  public static final String CLASSPATH_FXML_PLANE_DATA = CLASSPATH_FXML + "PlaneDataView.fxml";

  @Getter
  private final Resource mainView;
  @Getter
  private final Resource selectionView;
  @Getter
  private final Resource welcomeView;
  @Getter
  private final Resource settingsView;
  @Getter
  private final Resource metarView;
  @Getter
  private final Resource tafView;
  @Getter
  private final Resource tafTabView;
  @Getter
  private final Resource icaoView;
  @Getter
  private final Resource ofpView;
  @Getter
  private final Resource ofpTabView;
  @Getter
  private final Resource planeDataView;

  /**
   * Used for dependency injection. Injects the {@link Resource}s for the {@code FXML} files.
   *
   * @param mainView      {@link Resource} for {@code MainView.fxml}.
   * @param selectionView {@link Resource} for {@code SelectionView.fxml}.
   * @param welcomeView   {@link Resource} for {@code WelcomeView.fxml}.
   * @param settingsView  {@link Resource} for {@code SettingsView.fxml}.
   * @param metarView     {@link Resource} for {@code MetarView.fxml}.
   * @param tafView       {@link Resource} for {@code TafView.fxml}.
   * @param tafTabView    {@link Resource} for {@code TafTabView.fxml}.
   * @param icaoView      {@link Resource} for {@code IcaoView.fxml}.
   * @param ofpView       {@link Resource} for {@code OfpView.fxml}.
   * @param ofpTabView    {@link Resource} for {@code OfpTabView.fxml}.
   * @param planeDataView {@link Resource} for {@code PlaneDataView.fxml}.
   */
  @Contract(pure = true)
  @Autowired
  @Generated
  public FxmlResources(
      @Value(CLASSPATH_FXML_MAIN) Resource mainView,
      @Value(CLASSPATH_FXML_SELECTION) Resource selectionView,
      @Value(CLASSPATH_FXML_WELCOME) Resource welcomeView,
      @Value(CLASSPATH_FXML_SETTINGS) Resource settingsView,
      @Value(CLASSPATH_FXML_METAR) Resource metarView,
      @Value(CLASSPATH_FXML_TAF) Resource tafView,
      @Value(CLASSPATH_FXML_TAF_TAB) Resource tafTabView,
      @Value(CLASSPATH_FXML_ICAO) Resource icaoView,
      @Value(CLASSPATH_FXML_OFP) Resource ofpView,
      @Value(CLASSPATH_FXML_OFP_TAB) Resource ofpTabView,
      @Value(CLASSPATH_FXML_PLANE_DATA) Resource planeDataView) {
    this.mainView = mainView;
    this.selectionView = selectionView;
    this.welcomeView = welcomeView;
    this.settingsView = settingsView;
    this.metarView = metarView;
    this.tafView = tafView;
    this.tafTabView = tafTabView;
    this.icaoView = icaoView;
    this.ofpView = ofpView;
    this.ofpTabView = ofpTabView;
    this.planeDataView = planeDataView;
  }
}
