////////////////////////////////////////////////////////////////////////////////
// PiFly                                                                       /
// Copyright (C) 2021-2024 PiFly Team                                          /
//                                                                             /
// This file is part of PiFly.                                                 /
//                                                                             /
// PiFly is free software: you can redistribute it and/or modify               /
// it under the terms of the GNU General Public License as published by        /
// the Free Software Foundation, either version 3 of the License, or           /
// (at your option) any later version.                                         /
//                                                                             /
// PiFly is distributed in the hope that it will be useful,                    /
// but WITHOUT ANY WARRANTY; without even the implied warranty of              /
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               /
// GNU General Public License for more details.                                /
//                                                                             /
// You should have received a copy of the GNU General Public License           /
// along with PiFly. If not, see <http://www.gnu.org/licenses/>.               /
////////////////////////////////////////////////////////////////////////////////

package de.greluc.jfst.pifly.gui;

import com.pixelduke.transit.Style;
import com.pixelduke.transit.TransitStyleClass;
import com.pixelduke.transit.TransitTheme;
import de.greluc.jfst.pifly.event.ActiveViewEvent;
import de.greluc.jfst.pifly.event.BasePaneEvent;
import de.greluc.jfst.pifly.event.ViewChangeEvent;
import java.io.IOException;
import java.util.Optional;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import lombok.Generated;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.event.EventListener;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

/**
 * Loads the {@code FXMl} files and switches the shown view upon request.
 *
 * @author Lucas Greuloch (greluc, lucas.greuloch@protonmail.com)
 * @since 1.0.0
 * @version 1.2.1
 */
@Slf4j
@Component
public class ViewHandler {

  private final FxmlResources fxmlResources;
  private final ApplicationContext applicationContext;
  private final AlertHandler alertHandler;
  private GridPane basePane;
  private GridPane contentPane;

  /**
   * Used for dependency injection.
   *
   * @param fxmlResources Holds the resources for the {@code FXML} files.
   * @param applicationContext Spring Boot {@link ApplicationContext}.
   * @param alertHandler Used to show {@link javafx.scene.control.Alert}s.
   */
  @Contract(pure = true)
  @Autowired
  @Generated
  public ViewHandler(@NotNull @NonNull FxmlResources fxmlResources,
      @NotNull @NonNull ApplicationContext applicationContext,
      @NotNull @NonNull AlertHandler alertHandler) {
    this.fxmlResources = fxmlResources;
    this.applicationContext = applicationContext;
    this.alertHandler = alertHandler;
  }

  /**
   * Adds the {@code SelectionView} to the base Pane of the MainView and shows the {@code
   * WelcomeView} in it.
   *
   * @param event Contains the base pane of the {@code MainView}. Instance of a {@link VBox}.
   */
  @Generated
  @EventListener(BasePaneEvent.class)
  public void onBasePaneEvent(@NotNull @NonNull BasePaneEvent event) {
    if (basePane == null) {
      basePane = event.getBasePane();
      getPaneFromFxml(ViewType.SELECTION)
          .ifPresent(pane -> {
            GridPane.setConstraints(pane,0, 1);
            basePane.getChildren().add(pane);
            basePane.getStyleClass().add(TransitStyleClass.BACKGROUND);
            contentPane = (GridPane) pane.getChildren().get(0);
            onViewChangeEvent(new ViewChangeEvent(this, ViewType.WELCOME));
          });
    } else {
      log.warn("basePane is already set");
    }
  }

  /**
   * Changes the shown view. Publishes a {@link ActiveViewEvent} when a view was changed.
   *
   * @param event Specifies which {@code view} should be loaded/opened.
   */
  @EventListener(ViewChangeEvent.class)
  public void onViewChangeEvent(@NotNull @NonNull ViewChangeEvent event) {
    if (event.getViewType() == ViewType.SETTINGS) {
      getPaneFromFxml(ViewType.SETTINGS).ifPresentOrElse(this::openSettingsView, () -> {
        log.error("SettingsView could not be opened!");
        alertHandler.showGeneralError();
      });
    } else {
      contentPane.getChildren().clear();
      getPaneFromFxml(event.getViewType()).ifPresent(pane -> {
        GridPane.setVgrow(pane, Priority.ALWAYS);
        contentPane.getChildren().add(pane);
      });
      if (event.getViewType() != ViewType.WELCOME) {
        applicationContext.publishEvent(new ActiveViewEvent(this, event.getViewType()));
      }
    }
  }

  /**
   * Opens a new window with {@code SettingsView.fxml} as base {@link Pane}.
   *
   * @param pane Will be set as base {@link Pane} in the new window.
   */
  @Generated
  private void openSettingsView(@NotNull @NonNull Pane pane) {
    var stage = new Stage();
    var scene = new Scene(pane);
    new TransitTheme(scene, Style.DARK);
    pane.getStyleClass().add(TransitStyleClass.BACKGROUND);
    stage.setScene(scene);
    stage.setMaximized(false);
    stage.setResizable(true);
    stage.initModality(Modality.APPLICATION_MODAL);
    stage.show();
  }

  /**
   * Loads a {@code Pane} from a {@code FXML} document, which is specified by the given {@code
   * ViewType}.
   *
   * @param type Specifies which {@code FXML} should be loaded.
   *
   * @return {@link Pane} specified in the {@code FXML} wrapped in an {@link Optional}.
   */
  @NotNull
  public Optional<GridPane> getPaneFromFxml(@NotNull @NonNull ViewType type) {
    switch (type) {
      case METAR -> {
        log.debug("Trying to load MetarView");
        return getPaneFromFxml(fxmlResources.getMetarView());
      }
      case TAF -> {
        log.debug("Trying to load TafView");
        return getPaneFromFxml(fxmlResources.getTafView());
      }
      case TAF_TAB -> {
        log.debug("Trying to load TafTabView");
        return getPaneFromFxml(fxmlResources.getTafTabView());
      }
      case ICAO -> {
        log.debug("Trying to load IcaoView");
        return getPaneFromFxml(fxmlResources.getIcaoView());
      }
      case SETTINGS -> {
        log.debug("Trying to load SettingsView");
        return getPaneFromFxml(fxmlResources.getSettingsView());
      }
      case SELECTION -> {
        log.debug("Trying to load SelectionView");
        return getPaneFromFxml(fxmlResources.getSelectionView());
      }
      case OFP -> {
        log.debug("Trying to load OfpView");
        return getPaneFromFxml(fxmlResources.getOfpView());
      }
      case OFP_TAB -> {
        log.debug("Trying to load OfpTabView");
        return getPaneFromFxml(fxmlResources.getOfpTabView());
      }
      case PLANE_DATA -> {
        log.debug("Trying to load PlaneDataView");
        return getPaneFromFxml(fxmlResources.getPlaneDataView());
      }
      default -> {
        log.debug("Trying to load WelcomeView");
        return getPaneFromFxml(fxmlResources.getWelcomeView());
      }
    }
  }

  /**
   * Loads a {@link Pane} from a {@code FXML} document, which is specified by the given {@link
   * Resource} and wraps it in an {@link Optional}.
   *
   * @param resource Specifies which {@code FXML} should be loaded.
   *
   * @return {@code Pane} specified in the {@code FXML}.
   */
  @NotNull
  @Generated
  private Optional<GridPane> getPaneFromFxml(@NotNull @NonNull Resource resource) {
    try {
      var fxmlLoader = new FXMLLoader(resource.getURL());
      fxmlLoader.setControllerFactory(applicationContext::getBean);
      return Optional.of(fxmlLoader.load());
    } catch (IOException exception) {
      log.error("Exception while loading a FXML file. IOException!");
      log.debug("Tried to load following file: {}", resource.getFilename(), exception);
      if (log.isTraceEnabled()) {
        exception.printStackTrace();
      }
    } catch (Exception exception) {
      log.error("Exception while loading a FXML file. General exception!");
      log.debug("Tried to load following file: {}", resource.getFilename(), exception);
      if (log.isTraceEnabled()) {
        exception.printStackTrace();
      }
    }
    return Optional.empty();
  }
}
