////////////////////////////////////////////////////////////////////////////////
// PiFly                                                                       /
// Copyright (C) 2021-2024 PiFly Team                                          /
//                                                                             /
// This file is part of PiFly.                                                 /
//                                                                             /
// PiFly is free software: you can redistribute it and/or modify               /
// it under the terms of the GNU General Public License as published by        /
// the Free Software Foundation, either version 3 of the License, or           /
// (at your option) any later version.                                         /
//                                                                             /
// PiFly is distributed in the hope that it will be useful,                    /
// but WITHOUT ANY WARRANTY; without even the implied warranty of              /
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               /
// GNU General Public License for more details.                                /
//                                                                             /
// You should have received a copy of the GNU General Public License           /
// along with PiFly. If not, see <http://www.gnu.org/licenses/>.               /
////////////////////////////////////////////////////////////////////////////////

package de.greluc.jfst.pifly.gui.controller;

import de.greluc.jfst.pifly.data.IcaoData;
import de.greluc.jfst.pifly.event.IcaoCodeChangeEvent;
import de.greluc.jfst.pifly.i18n.I18N;
import de.greluc.jfst.pifly.i18n.I18NConstants;
import io.github.mivek.provider.airport.impl.DefaultAirportProvider;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.util.Duration;
import lombok.Generated;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.controlsfx.control.Notifications;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

/**
 * Corresponding controller for the IcaoView (IcaoView.fxml).
 * <p>
 * Shows an input to retrieve an ICAO code from the user, validates the input and notifies the
 * corresponding subscribers of the given ICAO code.
 *
 * @author Lucas Greuloch (greluc, lucas.greuloch@protonmail.com)
 * @since 1.0.0
 * @version 1.2.1
 */
@Component
@Slf4j
public class IcaoViewController {

  private final I18N i18N;
  private final ApplicationContext applicationContext;
  private final IcaoData icaoData;
  private final DefaultAirportProvider airportProvider = new DefaultAirportProvider();
  @FXML
  private ComboBox<String> icaoSelection;
  @FXML
  private Label labelAirport;
  @FXML
  private TextField textFieldIcaoCode;
  @FXML
  private Tooltip tooltipIcaoInput;

  /**
   * Used for dependency injection.
   *
   * @param i18N Class that handles the I18N in this project.
   * @param applicationContext Used to publish {@link IcaoCodeChangeEvent}s.
   * @param icaoData POJO that stores the different ICAO codes.
   */
  @Contract(pure = true)
  @Autowired
  @Generated
  public IcaoViewController(@NotNull @NonNull I18N i18N,
      @NotNull @NonNull ApplicationContext applicationContext,
      @NotNull @NonNull IcaoData icaoData) {
    this.i18N = i18N;
    this.applicationContext = applicationContext;
    this.icaoData = icaoData;
  }

  /**
   * Initializes the view. E.g. inits the text bindings for I18N.
   */
  @FXML
  @Generated
  private void initialize() {
    initTextBindings();
    initIcaoSelection();
  }

  private void initIcaoSelection() {
    ObservableList<String> icaoCodes = FXCollections.observableArrayList();
    icaoCodes.add(icaoData.getDeparture());
    icaoCodes.add(icaoData.getArrival());
    for (String alternate: icaoData.getAlternates()) {
      if (!icaoCodes.contains(alternate) && !alternate.isBlank()) {
        icaoCodes.add(alternate);
      }
    }
    if (!icaoCodes.contains(icaoData.getTakeoffAlternate()) && !icaoData.getTakeoffAlternate().isBlank()) {
      icaoCodes.add(icaoData.getTakeoffAlternate());
    }
    if (!icaoCodes.contains(icaoData.getEnrouteAlternate()) && !icaoData.getEnrouteAlternate().isBlank()) {
      icaoCodes.add(icaoData.getEnrouteAlternate());
    }
    icaoSelection.setItems(icaoCodes);
  }

  /**
   * Initializes the text bindings for I18N.
   */
  @Generated
  private void initTextBindings() {
    i18N.initBinding(labelAirport.textProperty(), I18NConstants.VIEW_ICAO_LABEL_AIRPORT);
    i18N.initBinding(textFieldIcaoCode.promptTextProperty(),
        I18NConstants.VIEW_ICAO_TEXTFIELD_PROMPT);
    i18N.initBinding(tooltipIcaoInput.textProperty(), I18NConstants.VIEW_ICAO_TEXTFIELD_TOOLTIP);
  }

  @FXML
  private void onIcaoSelected() {
    textFieldIcaoCode.setText(icaoSelection.getValue());
    icaoData.setManual(icaoSelection.getValue());
    applicationContext.publishEvent(new IcaoCodeChangeEvent(this));
  }

  /**
   * Called when the user enters a character into the {@link TextField}. Validates the input and
   * publishes a new {@link IcaoCodeChangeEvent} containing the new ICAO code when the input is
   * valid. If the given input has more than 4 characters it will be truncated to 4 characters.
   */
  @FXML
  @Generated
  private void updateIcaoCode() {
    String input = textFieldIcaoCode.getText().replaceAll("[\r\n]", "");
    input = input.toUpperCase(i18N.getLocale());
    textFieldIcaoCode.setText(input);
    textFieldIcaoCode.positionCaret(textFieldIcaoCode.getText().length());
    log.trace("Key pressed on textFieldIcaoCode: {}", input);
    if (input.length() > 4) {
      input = input.substring(0, 4);
      textFieldIcaoCode.setText(input);
    }
    textFieldIcaoCode.positionCaret(textFieldIcaoCode.getText().length());
    if (input.length() == 4) {
      if (isLatinLetter(input) && airportProvider.getAirports().containsKey(input)) {
        icaoData.setManual(input);
        icaoSelection.getSelectionModel().select(input);
        applicationContext.publishEvent(new IcaoCodeChangeEvent(this));
        log.debug("Published new ICAO code: {}", input);
      } else {
        log.debug("Input is invalid! Not an ICAO code!");
        Notifications.create()
            .title(i18N.get(I18NConstants.VIEW_ICAO_ERROR_TITLE))
            .text(i18N.get(I18NConstants.VIEW_ICAO_ERROR_CONTENT))
            .hideAfter(Duration.seconds(10))
            .showError();
      }
    }
  }

  /**
   * Checks if the given input {@code String} consists only latin characters.
   *
   * @param input {@code String} that should be checked.
   *
   * @return {@code true} if the input only consists latin letters. Otherwise {@code false}.
   */
  @Contract(pure = true)
  @Generated
  private boolean isLatinLetter(final @NotNull @NonNull String input) {
    return input.matches("^[a-zA-Z.]+$");
  }
}
