////////////////////////////////////////////////////////////////////////////////
// PiFly                                                                       /
// Copyright (C) 2021-2024 PiFly Team                                          /
//                                                                             /
// This file is part of PiFly.                                                 /
//                                                                             /
// PiFly is free software: you can redistribute it and/or modify               /
// it under the terms of the GNU General Public License as published by        /
// the Free Software Foundation, either version 3 of the License, or           /
// (at your option) any later version.                                         /
//                                                                             /
// PiFly is distributed in the hope that it will be useful,                    /
// but WITHOUT ANY WARRANTY; without even the implied warranty of              /
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               /
// GNU General Public License for more details.                                /
//                                                                             /
// You should have received a copy of the GNU General Public License           /
// along with PiFly. If not, see <http://www.gnu.org/licenses/>.               /
////////////////////////////////////////////////////////////////////////////////

package de.greluc.jfst.pifly.gui.controller;

import static de.greluc.jfst.pifly.i18n.I18NConstants.VIEW_MAIN_MENU_FILE;
import static de.greluc.jfst.pifly.i18n.I18NConstants.VIEW_MAIN_MENU_FILE_QUIT;
import static de.greluc.jfst.pifly.i18n.I18NConstants.VIEW_MAIN_MENU_FILE_SETTINGS;
import static de.greluc.jfst.pifly.i18n.I18NConstants.VIEW_MAIN_MENU_HELP;
import static de.greluc.jfst.pifly.i18n.I18NConstants.VIEW_MAIN_MENU_HELP_ABOUT;
import static de.greluc.jfst.pifly.i18n.I18NConstants.VIEW_MAIN_MENU_OFP;
import static de.greluc.jfst.pifly.i18n.I18NConstants.VIEW_MAIN_MENU_OFP_SIMBRIEF;
import static de.greluc.jfst.pifly.i18n.I18NConstants.VIEW_MAIN_MENU_OFP_SIMBRIEF_LOCAL;
import static de.greluc.jfst.pifly.i18n.I18NConstants.VIEW_MAIN_MENU_OFP_SIMBRIEF_ONLINE;

import de.greluc.jfst.pifly.data.PreferencesData;
import de.greluc.jfst.pifly.event.BasePaneEvent;
import de.greluc.jfst.pifly.event.ViewChangeEvent;
import de.greluc.jfst.pifly.gui.ViewHandler;
import de.greluc.jfst.pifly.gui.ViewType;
import de.greluc.jfst.pifly.i18n.I18N;
import de.greluc.jfst.pifly.service.PreferencesService;
import de.greluc.kfst.kfp.data.SourceType;
import javafx.application.Platform;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.scene.control.CheckMenuItem;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuItem;
import javafx.scene.layout.GridPane;
import lombok.Generated;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

/**
 * Corresponding controller for the MainView (MainView.fxml).
 *
 * @author Lucas Greuloch (greluc, lucas.greuloch@protonmail.com)
 * @version 1.2.1
 * @since 1.0.0
 */
@Component
@Slf4j
public class MainViewController {

  private final I18N i18N;
  private final ApplicationContext applicationContext;
  private final PreferencesData preferencesData;
  private final PreferencesService preferencesService;
  @FXML
  private GridPane basePane;
  @FXML
  private Menu menuFile;
  @FXML
  private MenuItem menuFileSettings;
  @FXML
  private MenuItem menuFileQuit;
  @FXML
  private Menu menuOfp;
  @FXML
  private Menu menuOfpSimbrief;
  @FXML
  private CheckMenuItem menuOfpSimbriefLocal;
  @FXML
  private CheckMenuItem menuOfpSimbriefOnline;
  @FXML
  private Menu menuHelp;
  @FXML
  private MenuItem menuHelpAbout;

  /**
   * Used for dependency injection.
   *
   * @param i18N Class that handles the I18N in this project.
   * @param applicationContext Spring Boot {@link ApplicationContext}.
   * @param preferencesData Singleton containing all preferences of the application.
   * @param preferencesService Service that persists and loads the preferences.
   */
  @Contract(pure = true)
  @Autowired
  @Generated
  public MainViewController(@NotNull @NonNull I18N i18N, ApplicationContext applicationContext,
      @NotNull @NonNull PreferencesData preferencesData,
      @NotNull @NonNull PreferencesService preferencesService) {
    this.applicationContext = applicationContext;
    this.i18N = i18N;
    this.preferencesData = preferencesData;
    this.preferencesService = preferencesService;
  }

  /**
   * Initializes the view. E.g. inits the text bindings for I18N and calls {@link ViewHandler} to
   * set the child pane.
   */
  @FXML
  @Generated
  void initialize() {
    applicationContext.publishEvent(new BasePaneEvent(this, basePane));
    if (preferencesData.getSimbriefSource() == SourceType.SIMBRIEF_FILE_XML) {
      menuOfpSimbriefLocal.setSelected(true);
      menuOfpSimbriefOnline.setSelected(false);
    } else if (preferencesData.getSimbriefSource() == SourceType.SIMBRIEF_API_XML) {
      menuOfpSimbriefLocal.setSelected(false);
      menuOfpSimbriefOnline.setSelected(true);
    }
    initTextBindings();
    i18N.setLocale();
  }

  /**
   * Initializes the text bindings for I18N.
   */
  @Generated
  private void initTextBindings() {
    i18N.initBinding(menuFile.textProperty(), VIEW_MAIN_MENU_FILE);
    i18N.initBinding(menuFileSettings.textProperty(), VIEW_MAIN_MENU_FILE_SETTINGS);
    i18N.initBinding(menuFileQuit.textProperty(), VIEW_MAIN_MENU_FILE_QUIT);
    i18N.initBinding(menuOfp.textProperty(), VIEW_MAIN_MENU_OFP);
    i18N.initBinding(menuOfpSimbrief.textProperty(), VIEW_MAIN_MENU_OFP_SIMBRIEF);
    i18N.initBinding(menuOfpSimbriefLocal.textProperty(), VIEW_MAIN_MENU_OFP_SIMBRIEF_LOCAL);
    i18N.initBinding(menuOfpSimbriefOnline.textProperty(), VIEW_MAIN_MENU_OFP_SIMBRIEF_ONLINE);
    i18N.initBinding(menuHelp.textProperty(), VIEW_MAIN_MENU_HELP);
    i18N.initBinding(menuHelpAbout.textProperty(), VIEW_MAIN_MENU_HELP_ABOUT);
  }

  /**
   * Used to open a new window for the settings view, when the user pressed the corresponding
   * {@link MenuItem}.
   */
  @FXML
  @Generated
  private void openSettingsView() {
    applicationContext.publishEvent(new ViewChangeEvent(this, ViewType.SETTINGS));
  }

  /**
   * Used to switch the OFP source, when the user pressed the corresponding {@link CheckMenuItem}.
   *
   * @param event The event that triggers this method. Contains the menu button that was pressed
   *     as source.
   */
  @FXML
  @Generated
  private void changeOfpSource(@NotNull @NonNull Event event) {
    if (event.getSource().equals(menuOfpSimbriefLocal)) {
      preferencesData.setSimbriefSource(SourceType.SIMBRIEF_FILE_XML);
      menuOfpSimbriefOnline.setSelected(false);
    } else if (event.getSource().equals(menuOfpSimbriefOnline)) {
      preferencesData.setSimbriefSource(SourceType.SIMBRIEF_API_XML);
      menuOfpSimbriefLocal.setSelected(false);
    }
    preferencesService.persistPreferences();
  }

  /**
   * Shuts the application down.
   */
  @FXML
  @Generated
  private void closeApplication() {
    Platform.exit();
  }
}