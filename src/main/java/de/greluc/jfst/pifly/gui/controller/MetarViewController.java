////////////////////////////////////////////////////////////////////////////////
// PiFly                                                                       /
// Copyright (C) 2021-2024 PiFly Team                                          /
//                                                                             /
// This file is part of PiFly.                                                 /
//                                                                             /
// PiFly is free software: you can redistribute it and/or modify               /
// it under the terms of the GNU General Public License as published by        /
// the Free Software Foundation, either version 3 of the License, or           /
// (at your option) any later version.                                         /
//                                                                             /
// PiFly is distributed in the hope that it will be useful,                    /
// but WITHOUT ANY WARRANTY; without even the implied warranty of              /
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               /
// GNU General Public License for more details.                                /
//                                                                             /
// You should have received a copy of the GNU General Public License           /
// along with PiFly. If not, see <http://www.gnu.org/licenses/>.               /
////////////////////////////////////////////////////////////////////////////////

package de.greluc.jfst.pifly.gui.controller;

import static de.greluc.jfst.pifly.i18n.I18NConstants.VIEW_METAR_AIRPORT_NAME;
import static de.greluc.jfst.pifly.i18n.I18NConstants.VIEW_METAR_ALTIMETER;
import static de.greluc.jfst.pifly.i18n.I18NConstants.VIEW_METAR_AREA_PROMPT;
import static de.greluc.jfst.pifly.i18n.I18NConstants.VIEW_METAR_CLOUD_COVERAGE;
import static de.greluc.jfst.pifly.i18n.I18NConstants.VIEW_METAR_CLOUD_COVERAGE_BKN;
import static de.greluc.jfst.pifly.i18n.I18NConstants.VIEW_METAR_CLOUD_COVERAGE_CAVOK;
import static de.greluc.jfst.pifly.i18n.I18NConstants.VIEW_METAR_CLOUD_COVERAGE_FEW;
import static de.greluc.jfst.pifly.i18n.I18NConstants.VIEW_METAR_CLOUD_COVERAGE_NSC;
import static de.greluc.jfst.pifly.i18n.I18NConstants.VIEW_METAR_CLOUD_COVERAGE_OVC;
import static de.greluc.jfst.pifly.i18n.I18NConstants.VIEW_METAR_CLOUD_COVERAGE_SCT;
import static de.greluc.jfst.pifly.i18n.I18NConstants.VIEW_METAR_CLOUD_COVERAGE_SKC;
import static de.greluc.jfst.pifly.i18n.I18NConstants.VIEW_METAR_CLOUD_HEIGHT;
import static de.greluc.jfst.pifly.i18n.I18NConstants.VIEW_METAR_CLOUD_UNIT_FEET;
import static de.greluc.jfst.pifly.i18n.I18NConstants.VIEW_METAR_DEWPOINT;
import static de.greluc.jfst.pifly.i18n.I18NConstants.VIEW_METAR_PHENOMENON;
import static de.greluc.jfst.pifly.i18n.I18NConstants.VIEW_METAR_TEMPERATURE;
import static de.greluc.jfst.pifly.i18n.I18NConstants.VIEW_METAR_TIME;
import static de.greluc.jfst.pifly.i18n.I18NConstants.VIEW_METAR_VISIBILITY;
import static de.greluc.jfst.pifly.i18n.I18NConstants.VIEW_METAR_WIND;
import static de.greluc.jfst.pifly.i18n.I18NConstants.VIEW_METAR_WIND_DIRECTION_FIXED;
import static de.greluc.jfst.pifly.i18n.I18NConstants.VIEW_METAR_WIND_DIRECTION_RANGE;
import static de.greluc.jfst.pifly.i18n.I18NConstants.VIEW_METAR_WIND_DIRECTION_VARYING;
import static de.greluc.jfst.pifly.i18n.I18NConstants.VIEW_METAR_WIND_SPEED;
import static de.greluc.jfst.pifly.i18n.I18NConstants.VIEW_METAR_WIND_SPEED_GUSTS;

import de.greluc.jfst.pifly.data.IcaoData;
import de.greluc.jfst.pifly.event.ActiveViewEvent;
import de.greluc.jfst.pifly.event.IcaoCodeChangeEvent;
import de.greluc.jfst.pifly.event.LocaleChangeEvent;
import de.greluc.jfst.pifly.event.MetarRequestEvent;
import de.greluc.jfst.pifly.event.MetarResponseEvent;
import de.greluc.jfst.pifly.event.StartWeatherUpdateEvent;
import de.greluc.jfst.pifly.event.StopWeatherUpdateEvent;
import de.greluc.jfst.pifly.gui.ViewHandler;
import de.greluc.jfst.pifly.gui.ViewType;
import de.greluc.jfst.pifly.i18n.I18N;
import io.github.mivek.enums.Phenomenon;
import io.github.mivek.model.Metar;
import java.time.format.DateTimeFormatter;
import java.util.Objects;
import javafx.application.Platform;
import javafx.beans.property.StringProperty;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.layout.GridPane;
import lombok.Generated;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

/**
 * Corresponding controller for the MetarView (MetarView.fxml).
 * <p>
 * Shows the IcaoView to get an ICAO code and displays the retrieved and decoded TAF to the user.
 * Uses {@link MetarRequestEvent} and {@link MetarResponseEvent} to retrieve a METAR.
 *
 * @author Lucas Greuloch (greluc, lucas.greuloch@protonmail.com)
 * @version 1.2.1
 * @since 1.0.0
 */
@Component
@Slf4j
public class MetarViewController {

  private final I18N i18N;
  private final ViewHandler viewHandler;
  private final ApplicationContext applicationContext;
  private final IcaoData icaoData;
  private boolean isActive;
  private Metar metar = null;
  @FXML
  private GridPane pane;
  @FXML
  private TextArea textAreaMetarValue;
  @FXML
  private Label labelAirportName;
  @FXML
  private Label labelAirportNameValue;
  @FXML
  private Label labelAltimeter;
  @FXML
  private Label labelAltimeterValue;
  @FXML
  private Label labelWind;
  @FXML
  private Label labelWindValue;
  @FXML
  private Label labelTemperature;
  @FXML
  private Label labelTemperatureValue;
  @FXML
  private Label labelTime;
  @FXML
  private Label labelTimeValue;
  @FXML
  private Label labelCloudCoverage;
  @FXML
  private Label labelCloudCoverageValue;
  @FXML
  private Label labelCloudHeight;
  @FXML
  private Label labelCloudHeightValue;
  @FXML
  private Label labelDewPoint;
  @FXML
  private Label labelDewPointValue;
  @FXML
  private Label labelVisibility;
  @FXML
  private Label labelVisibilityValue;
  @FXML
  private Label labelPhenomenon;
  @FXML
  private Label labelPhenomenonValue;

  /**
   * Used for dependency injection.
   *
   * @param i18N Class that handles the I18N in this project.
   * @param viewHandler Required to set the View.
   * @param applicationContext Spring Boot {@link ApplicationContext} used for event.
   * @param icaoData POJO that stores the different ICAO codes.
   */
  @Contract(pure = true)
  @Autowired
  @Generated
  public MetarViewController(@NotNull @NonNull I18N i18N,
      @NotNull @NonNull ViewHandler viewHandler,
      @NotNull @NonNull ApplicationContext applicationContext,
      @NotNull @NonNull IcaoData icaoData) {
    this.i18N = i18N;
    this.viewHandler = viewHandler;
    this.applicationContext = applicationContext;
    this.icaoData = icaoData;
  }

  /**
   * Initializes the view. E.g. sets the IcaoView as a child and inits the text bindings for I18N.
   */
  @FXML
  @Generated
  public void initialize() {
    viewHandler.getPaneFromFxml(ViewType.ICAO).ifPresent(childPane -> pane.add(childPane, 0, 0, 3, 1));
    initTextBindings();
  }

  /**
   * Initializes the text bindings for I18N.
   */
  @SuppressWarnings("DuplicatedCode")
  @Generated
  private void initTextBindings() {
    i18N.initBinding(labelAirportName.textProperty(), VIEW_METAR_AIRPORT_NAME);
    i18N.initBinding(labelAltimeter.textProperty(), VIEW_METAR_ALTIMETER);
    i18N.initBinding(labelWind.textProperty(), VIEW_METAR_WIND);
    i18N.initBinding(labelTemperature.textProperty(), VIEW_METAR_TEMPERATURE);
    i18N.initBinding(labelDewPoint.textProperty(), VIEW_METAR_DEWPOINT);
    i18N.initBinding(labelTime.textProperty(), VIEW_METAR_TIME);
    i18N.initBinding(labelCloudCoverage.textProperty(), VIEW_METAR_CLOUD_COVERAGE);
    i18N.initBinding(labelCloudHeight.textProperty(), VIEW_METAR_CLOUD_HEIGHT);
    i18N.initBinding(labelVisibility.textProperty(), VIEW_METAR_VISIBILITY);
    i18N.initBinding(labelPhenomenon.textProperty(), VIEW_METAR_PHENOMENON);
    i18N.initBinding(textAreaMetarValue.promptTextProperty(), VIEW_METAR_AREA_PROMPT);
  }

  /**
   * Notifies the view that the user has selected a new ICAO code.
   */
  @EventListener(IcaoCodeChangeEvent.class)
  public void onIcaoCodeChangeEvent() {
    if (isActive) {
      applicationContext.publishEvent(new StartWeatherUpdateEvent(this, icaoData.getManual(), true));
    }
  }

  /**
   * Notifies the view that a new view is currently shown in the application.
   * <p>
   * Used to determine whether the view needs to query the METAR or not.
   *
   * @param event Contains the type of the currently shown view.
   */
  @EventListener(ActiveViewEvent.class)
  public void onActiveViewEvent(@NotNull @NonNull ActiveViewEvent event) {
    isActive = event.getType().equals(ViewType.METAR);
    if (!isActive) {
      applicationContext.publishEvent(new StopWeatherUpdateEvent(this));
    }
  }

  /**
   * Notifies the view that a new METAR has been received.
   *
   * @param event Contains the newly received METAR.
   */
  @EventListener(MetarResponseEvent.class)
  public void onMetarResponseEvent(@NotNull @NonNull MetarResponseEvent event) {
    metar = event.getMetar();
    setMetar();
  }

  /**
   * Notifies the view that a new METAR has been received.
   *
   * @param ignoredEvent Ignored.
   */
  @EventListener(LocaleChangeEvent.class)
  public void onLocaleChangeEvent(@NotNull @NonNull LocaleChangeEvent ignoredEvent) {
    setMetar();
  }

  private void setMetar() {
    Platform.runLater(() -> {
      if (metar == null) {
        return;
      }
      textAreaMetarValue.setText(metar.getMessage());
      labelAirportNameValue.setText(metar.getAirport().getName().replace(" Airport", ""));
      labelVisibilityValue.setText(metar.getVisibility().getMainVisibility());
      setPhenomenon(metar, labelPhenomenonValue.textProperty());
      setAltimeter(metar, labelAltimeterValue.textProperty());
      setWind(metar, labelWindValue.textProperty());
      setIssueTime(metar, labelTimeValue.textProperty());
      setCloudCoverage(metar, labelCloudCoverageValue.textProperty());
      setCloudsHeight(metar, labelCloudHeightValue.textProperty());
      setTemperature(metar, labelTemperatureValue.textProperty(), false);
      setTemperature(metar, labelDewPointValue.textProperty(), true);
    });
  }

  private void setPhenomenon(@NotNull @NonNull Metar metar, @NotNull @NonNull StringProperty textProperty) {
    StringBuilder text = new StringBuilder();
    if (!metar.getWeatherConditions().isEmpty()) {
      if (metar.getWeatherConditions().get(0).getIntensity() != null) {
        text.append(metar.getWeatherConditions().get(0).getIntensity().toString()).append(" ");
      }
      if (metar.getWeatherConditions().get(0).getDescriptive() != null) {
        text.append(metar.getWeatherConditions().get(0).getDescriptive().toString()).append(" ");
      }
      if (!metar.getWeatherConditions().get(0).getPhenomenons().isEmpty()
          && metar.getWeatherConditions().get(0).getPhenomenons().get(0) != null) {
        for (Phenomenon phenomenon: metar.getWeatherConditions().get(0).getPhenomenons()) {
          text.append(phenomenon.toString()).append(" ");
        }
      }
    }
    textProperty.set(text.toString());
  }

  /**
   * Reads the altimeter in hPA from the {@link Metar}. Conversion to inHG needs to be implemented
   * in MetarParser.
   *
   * @param metar Contains the altimeter in hPA.
   * @param stringProperty The aim of the operation.
   */
  void setAltimeter(@NotNull @NonNull Metar metar,
      @NotNull @NonNull StringProperty stringProperty) {
    stringProperty
        .set(String.format("%d hPa", metar.getAltimeter())); // MetarParser only supports hPa.
  }

  /**
   * Reads the temperature in °C from the {@link Metar}.
   *
   * @param metar Contains the temperature in °C.
   * @param stringProperty The aim of the operation.
   */
  public void setTemperature(@NotNull @NonNull Metar metar,
      @NotNull @NonNull StringProperty stringProperty,
      boolean isDewPoint) {
    if (isDewPoint) {
      stringProperty
          .set(String.format("%d°C", metar.getDewPoint())); // MetarParser only supports °C.
    } else {
      stringProperty
          .set(String.format("%d°C", metar.getTemperature())); // MetarParser only supports °C.
    }
  }

  /**
   * Reads the lower border height of the clouds from the {@link Metar} in feet AGL.
   *
   * @param metar Contains the height of the clouds in feet.
   * @param stringProperty The aim of the operation.
   */
  void setCloudsHeight(@NotNull @NonNull Metar metar,
      @NotNull @NonNull StringProperty stringProperty) {
    stringProperty.unbind();
    if (metar.getClouds().isEmpty()) {
      stringProperty.set("");
      return;
    }
    i18N.initBinding(stringProperty, VIEW_METAR_CLOUD_UNIT_FEET,
        metar.getClouds().get(0).getHeight());
  }

  /**
   * Reads the coverage of the clouds from the {@link Metar}. Shows the code word (e.g. FEW) and the
   * value in eighths.
   *
   * @param metar Contains the coverage of the clouds.
   * @param stringProperty The aim of the operation.
   */
  void setCloudCoverage(@NotNull @NonNull Metar metar,
      @NotNull @NonNull StringProperty stringProperty) {
    stringProperty.unbind();
    if (metar.getClouds().isEmpty()) {
      if (metar.isCavok()) {
        i18N.initBinding(stringProperty, VIEW_METAR_CLOUD_COVERAGE_CAVOK);
      } else {
        stringProperty.set("");
      }
      return;
    }
    switch (metar.getClouds().get(0).getQuantity()) {
      case OVC -> i18N.initBinding(stringProperty, VIEW_METAR_CLOUD_COVERAGE_OVC);
      case BKN -> i18N.initBinding(stringProperty, VIEW_METAR_CLOUD_COVERAGE_BKN);
      case SCT -> i18N.initBinding(stringProperty, VIEW_METAR_CLOUD_COVERAGE_SCT);
      case FEW -> i18N.initBinding(stringProperty, VIEW_METAR_CLOUD_COVERAGE_FEW);
      case NSC -> i18N.initBinding(stringProperty, VIEW_METAR_CLOUD_COVERAGE_NSC);
      default -> i18N.initBinding(stringProperty, VIEW_METAR_CLOUD_COVERAGE_SKC);
    }
  }

  /**
   * Reads the time when the METAR was issued from the {@link Metar}.
   *
   * @param metar Contains the issue time.
   * @param stringProperty The aim of the operation.
   */
  void setIssueTime(@NotNull @NonNull Metar metar,
      @NotNull @NonNull StringProperty stringProperty) {
    stringProperty
        .set(String.format("%sZ", metar.getTime().format(DateTimeFormatter.ofPattern("HH:mm"))));
  }

  /**
   * Reads the wind data from the {@link Metar}.
   *
   * @param data Contains the wind data.
   * @param stringProperty The aim of the operation.
   */
  private void setWind(@NotNull @NonNull Metar data,
      @NotNull @NonNull StringProperty stringProperty) {
    stringProperty.set(setWindDirection(data) + " " + setWindSpeed(data));
  }

  /**
   * Reads the direction of the wind from the {@link Metar}.
   *
   * @param metar Contains the wind information.
   */
  String setWindDirection(@NotNull @NonNull Metar metar) {
    if (metar.getWind().getDirectionDegrees() == null) {
      return i18N.get(VIEW_METAR_WIND_DIRECTION_VARYING);
    } else {
      if (!Objects.equals(metar.getWind().getMinVariation(), metar.getWind().getMaxVariation())) {
        return i18N.get(VIEW_METAR_WIND_DIRECTION_RANGE, metar.getWind().getDirectionDegrees(),
            metar.getWind().getMinVariation(), metar.getWind().getMaxVariation());
      } else {
        return i18N.get(VIEW_METAR_WIND_DIRECTION_FIXED, metar.getWind().getDirectionDegrees());
      }
    }
  }

  /**
   * Reads the speed of the wind from the {@link Metar}.
   *
   * @param metar Contains the wind information.
   */
  String setWindSpeed(@NotNull @NonNull Metar metar) {
    if (metar.getWind().getGust() != null && metar.getWind().getGust() > 0) {
      return i18N
          .get(VIEW_METAR_WIND_SPEED_GUSTS, metar.getWind().getSpeed(), metar.getWind().getUnit(),
              metar.getWind().getGust());
    } else {
      return i18N.get(VIEW_METAR_WIND_SPEED, metar.getWind().getSpeed(), metar.getWind().getUnit());
    }
  }
}