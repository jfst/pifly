////////////////////////////////////////////////////////////////////////////////
// PiFly                                                                       /
// Copyright (C) 2021-2024 PiFly Team                                          /
//                                                                             /
// This file is part of PiFly.                                                 /
//                                                                             /
// PiFly is free software: you can redistribute it and/or modify               /
// it under the terms of the GNU General Public License as published by        /
// the Free Software Foundation, either version 3 of the License, or           /
// (at your option) any later version.                                         /
//                                                                             /
// PiFly is distributed in the hope that it will be useful,                    /
// but WITHOUT ANY WARRANTY; without even the implied warranty of              /
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               /
// GNU General Public License for more details.                                /
//                                                                             /
// You should have received a copy of the GNU General Public License           /
// along with PiFly. If not, see <http://www.gnu.org/licenses/>.               /
////////////////////////////////////////////////////////////////////////////////

package de.greluc.jfst.pifly.gui.controller;

import de.greluc.jfst.pifly.data.OfpData;
import de.greluc.jfst.pifly.i18n.I18N;
import de.greluc.jfst.pifly.i18n.I18NConstants;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import lombok.Generated;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * Corresponding controller for the OfpTabView (OfpTabView.fxml).
 *
 * @author Lucas Greuloch (greluc, lucas.greuloch@protonmail.com)
 * @since 1.0.0
 * @version 1.2.1
 */
@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@Slf4j
public class OfpTabViewController {

  private final I18N i18N;
  private final OfpViewController ofpViewController;
  private final OfpData ofpData;
  @FXML
  private Label labelIcao;
  @FXML
  private Label labelIcaoValue;
  @FXML
  private Label labelName;
  @FXML
  private Label labelNameValue;
  @FXML
  private Label labelRunway;
  @FXML
  private Label labelRunwayValue;
  @FXML
  private Label labelMetar;
  @FXML
  private Label labelMetarValue;
  @FXML
  private Label labelTaf;
  @FXML
  private Label labelTafValue;

  /**
   * Used for dependency injection.
   *
   * @param i18N Class that handles the I18N in this project.
   * @param ofpViewController Controller that delivers the TAF trend data.
   * @param ofpData POJO that stores the OFP.
   */
  @Contract(pure = true)
  @Autowired
  @Generated
  public OfpTabViewController(@NotNull @NonNull I18N i18N,
      @NotNull @NonNull OfpViewController ofpViewController,
      @NotNull @NonNull OfpData ofpData) {
    this.i18N = i18N;
    this.ofpViewController = ofpViewController;
    this.ofpData = ofpData;
  }

  /**
   * Initializes the view. E.g. sets the IcaoView as a child and inits the text bindings for I18N.
   */
  @FXML
  @Generated
  public void initialize() {
    initTextBindings();
    switch (ofpViewController.getAlternatesCounter()) {
      case 1, 2, 3, 4 -> {
        labelIcaoValue.setText(ofpData.getOfp().getAirports().getAlternates().get(ofpViewController.getAlternatesCounter() - 1).getIcaoCode());
        labelNameValue.setText(trimName(ofpData.getOfp().getAirports().getAlternates().get(ofpViewController.getAlternatesCounter() - 1).getName()));
        labelRunwayValue.setText(ofpData.getOfp().getAirports().getAlternates().get(ofpViewController.getAlternatesCounter() - 1).getRunway());
        labelMetarValue.setText(ofpData.getOfp().getAirports().getAlternates().get(ofpViewController.getAlternatesCounter() - 1).getMetar().replace("\n", " "));
        labelTafValue.setText(ofpData.getOfp().getAirports().getAlternates().get(ofpViewController.getAlternatesCounter() - 1).getTaf());
      }
      case 5 -> {
        labelIcaoValue.setText(ofpData.getOfp().getAirports().getTakeOffAlternate().getIcaoCode());
        labelNameValue.setText(trimName(ofpData.getOfp().getAirports().getTakeOffAlternate().getName()));
        labelRunwayValue.setText(ofpData.getOfp().getAirports().getTakeOffAlternate().getRunway());
        labelMetarValue.setText(ofpData.getOfp().getAirports().getTakeOffAlternate().getMetar().replace("\n", " "));
        labelTafValue.setText(ofpData.getOfp().getAirports().getTakeOffAlternate().getTaf());
      }
      case 6 -> {
        labelIcaoValue.setText(ofpData.getOfp().getAirports().getEnrouteAlternate().getIcaoCode());
        labelNameValue.setText(trimName(ofpData.getOfp().getAirports().getEnrouteAlternate().getName()));
        labelRunwayValue.setText(ofpData.getOfp().getAirports().getTakeOffAlternate().getRunway());
        labelMetarValue.setText(ofpData.getOfp().getAirports().getTakeOffAlternate().getMetar().replace("\n", " "));
        labelTafValue.setText(ofpData.getOfp().getAirports().getTakeOffAlternate().getTaf());
      }
      default -> {
        // no action taken because we can only have 4 alternates + 1 takeoff alternate + 1 enroute alternate
        // etops not used at the moment
      }
    }
  }

  @Contract(pure = true)
  private @NotNull String trimName(@NotNull @NonNull String input) {
    return input.replace(" Airport", "");
  }

  /**
   * Initializes the text bindings for I18N.
   */
  @Generated
  private void initTextBindings() {
    i18N.initBinding(labelIcao.textProperty(), I18NConstants.VIEW_OFP_TAB_LABEL_ICAO);
    i18N.initBinding(labelName.textProperty(), I18NConstants.VIEW_OFP_TAB_LABEL_NAME);
    i18N.initBinding(labelRunway.textProperty(), I18NConstants.VIEW_OFP_TAB_LABEL_RUNWAY);
    i18N.initBinding(labelMetar.textProperty(), I18NConstants.VIEW_OFP_TAB_LABEL_METAR);
    i18N.initBinding(labelTaf.textProperty(), I18NConstants.VIEW_OFP_TAB_LABEL_TAF);
  }
}
