////////////////////////////////////////////////////////////////////////////////
// PiFly                                                                       /
// Copyright (C) 2021-2024 PiFly Team                                          /
//                                                                             /
// This file is part of PiFly.                                                 /
//                                                                             /
// PiFly is free software: you can redistribute it and/or modify               /
// it under the terms of the GNU General Public License as published by        /
// the Free Software Foundation, either version 3 of the License, or           /
// (at your option) any later version.                                         /
//                                                                             /
// PiFly is distributed in the hope that it will be useful,                    /
// but WITHOUT ANY WARRANTY; without even the implied warranty of              /
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               /
// GNU General Public License for more details.                                /
//                                                                             /
// You should have received a copy of the GNU General Public License           /
// along with PiFly. If not, see <http://www.gnu.org/licenses/>.               /
////////////////////////////////////////////////////////////////////////////////

package de.greluc.jfst.pifly.gui.controller;

import static de.greluc.jfst.pifly.i18n.I18NConstants.VIEW_OFP_BUTTON_LOAD;
import static de.greluc.jfst.pifly.i18n.I18NConstants.VIEW_OFP_BUTTON_RESET;
import static de.greluc.jfst.pifly.i18n.I18NConstants.VIEW_OFP_ERROR_CONTENT;
import static de.greluc.jfst.pifly.i18n.I18NConstants.VIEW_OFP_ERROR_TITLE;
import static de.greluc.jfst.pifly.i18n.I18NConstants.VIEW_OFP_LABEL_ARRIVAL;
import static de.greluc.jfst.pifly.i18n.I18NConstants.VIEW_OFP_LABEL_ARRIVAL_ICAO;
import static de.greluc.jfst.pifly.i18n.I18NConstants.VIEW_OFP_LABEL_ARRIVAL_NAME;
import static de.greluc.jfst.pifly.i18n.I18NConstants.VIEW_OFP_LABEL_ARRIVAL_PROCEDURE;
import static de.greluc.jfst.pifly.i18n.I18NConstants.VIEW_OFP_LABEL_ARRIVAL_RUNWAY;
import static de.greluc.jfst.pifly.i18n.I18NConstants.VIEW_OFP_LABEL_ARRIVAL_TIME;
import static de.greluc.jfst.pifly.i18n.I18NConstants.VIEW_OFP_LABEL_AVERAGE_WC;
import static de.greluc.jfst.pifly.i18n.I18NConstants.VIEW_OFP_LABEL_DEPARTURE;
import static de.greluc.jfst.pifly.i18n.I18NConstants.VIEW_OFP_LABEL_DEPARTURE_ICAO;
import static de.greluc.jfst.pifly.i18n.I18NConstants.VIEW_OFP_LABEL_DEPARTURE_NAME;
import static de.greluc.jfst.pifly.i18n.I18NConstants.VIEW_OFP_LABEL_DEPARTURE_PROCEDURE;
import static de.greluc.jfst.pifly.i18n.I18NConstants.VIEW_OFP_LABEL_DEPARTURE_RUNWAY;
import static de.greluc.jfst.pifly.i18n.I18NConstants.VIEW_OFP_LABEL_DEPARTURE_TIME;
import static de.greluc.jfst.pifly.i18n.I18NConstants.VIEW_OFP_LABEL_ROUTE;
import static de.greluc.jfst.pifly.i18n.I18NConstants.VIEW_OFP_LABEL_TIME_ENROUTE;
import static de.greluc.jfst.pifly.i18n.I18NConstants.VIEW_OFP_LABEL_TIME_ENROUTE_VALUE;
import static de.greluc.kfst.kfp.data.SourceType.SIMBRIEF_API_XML;
import static de.greluc.kfst.kfp.data.SourceType.SIMBRIEF_FILE_XML;

import de.greluc.jfst.pifly.data.IcaoData;
import de.greluc.jfst.pifly.data.OfpData;
import de.greluc.jfst.pifly.data.PreferencesData;
import de.greluc.jfst.pifly.gui.ViewHandler;
import de.greluc.jfst.pifly.gui.ViewType;
import de.greluc.jfst.pifly.i18n.I18N;
import de.greluc.kfst.kfp.data.generic.GenericOFP;
import de.greluc.kfst.kfp.service.OfpService;
import jakarta.annotation.PreDestroy;
import java.io.File;
import java.time.format.DateTimeFormatter;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.util.Duration;
import lombok.Generated;
import lombok.Getter;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.controlsfx.control.Notifications;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Corresponding controller for the OfpView (OfpView.fxml).
 * <p>
 * Loads a OFP and displays the most important information from it.
 *
 * @author Lucas Greuloch (greluc, lucas.greuloch@protonmail.com)
 * @version 1.2.1
 * @since 1.0.0
 */
@Component
@Slf4j
public class OfpViewController {
  private final ExecutorService executorService = Executors.newSingleThreadExecutor();

  private final I18N i18N;
  private final ViewHandler viewHandler;
  private final OfpService ofpService;
  private final OfpData ofpData;
  private final PreferencesData preferencesData;
  private final IcaoData icaoData;
  @Getter
  private int alternatesCounter = 0;
  @FXML
  private TabPane tabPane;
  @FXML
  private Button buttonLoad;
  @FXML
  private Button buttonReset;
  @FXML
  private Label labelDeparture;
  @FXML
  private Label labelDepartureIcao;
  @FXML
  private Label labelDepartureIcaoValue;
  @FXML
  public Label labelDepartureName;
  @FXML
  private Label labelDepartureNameValue;
  @FXML
  public Label labelDepartureTime;
  @FXML
  private Label labelDepartureTimeValue;
  @FXML
  public Label labelDepartureRunway;
  @FXML
  private Label labelDepartureRunwayValue;
  @FXML
  public Label labelDepartureProcedure;
  @FXML
  private Label labelDepartureProcedureValue;
  @FXML
  public Label labelArrival;
  @FXML
  private Label labelArrivalIcao;
  @FXML
  private Label labelArrivalIcaoValue;
  @FXML
  private Label labelArrivalName;
  @FXML
  private Label labelArrivalNameValue;
  @FXML
  private Label labelArrivalTime;
  @FXML
  private Label labelArrivalTimeValue;
  @FXML
  private Label labelArrivalRunway;
  @FXML
  private Label labelArrivalRunwayValue;
  @FXML
  private Label labelArrivalProcedure;
  @FXML
  private Label labelArrivalProcedureValue;
  @FXML
  private Label labelRoute;
  @FXML
  private Label labelRouteValue;
  @FXML
  private Label labelTimeEnroute;
  @FXML
  private Label labelTimeEnrouteValue;
  @FXML
  private Label labelAverageWC;
  @FXML
  private Label labelAverageWCValue;

  /**
   * Used for dependency injection.
   *
   * @param i18N            Class that handles the I18N in this project.
   * @param viewHandler     Required to set the View.
   * @param ofpService      Loads the OFP from a SimBrief XML file and returns it as a
   *                        {@link GenericOFP} object.
   * @param ofpData         POJO that stores the OFP.
   * @param preferencesData Singleton containing all preferences of the application.
   * @param icaoData        POJO that stores the different ICAO codes.
   */
  @Contract(pure = true)
  @Autowired
  public OfpViewController(@NotNull @NonNull I18N i18N,
                           @NotNull @NonNull ViewHandler viewHandler,
                           @NotNull @NonNull OfpService ofpService,
                           @NotNull @NonNull OfpData ofpData, PreferencesData preferencesData,
                           @NotNull @NonNull IcaoData icaoData) {
    this.i18N = i18N;
    this.viewHandler = viewHandler;
    this.ofpService = ofpService;
    this.ofpData = ofpData;
    this.preferencesData = preferencesData;
    this.icaoData = icaoData;
  }

  /**
   * Initializes the view.
   */
  @FXML
  @Generated
  void initialize() {
    initTextBindings();
    if (ofpData.isPresent()) {
      log.debug("OFP Data is present. Populating the OFPView.");
      populateView();
    }
  }

  @FXML
  private void onButtonLoadPressed() {
    log.trace("Load OFP button in OFPView has been pressed.");
    loadOfp();
  }

  @FXML
  private void onButtonResetPressed() {
    log.trace("Reset OFP button in OFPView has been pressed.");
    resetViewData();
    resetDataSingletons();
  }

  /**
   * Loads the OFP by using the JFlightPlan library.
   */
  private void loadOfp() {
    log.debug("Trying to load the OFP.");
    log.debug("Resetting data singletons.");
    resetDataSingletons();
    File file;
    if (preferencesData.getSimbriefSource().equals(SIMBRIEF_FILE_XML)) {
      file = showFileChooser();
    } else {
      file = null;
    }
    executorService.submit(() -> {
      GenericOFP ofp;
      if (preferencesData.getSimbriefSource().equals(SIMBRIEF_FILE_XML)) {
        if (file != null) {
          ofp = ofpService.loadOfp(preferencesData.getSimbriefSource(), file, preferencesData.getSimbriefId(), false);
        } else {
          ofp = null;
        }
      } else if (preferencesData.getSimbriefSource().equals(SIMBRIEF_API_XML)) {
        ofp = ofpService.loadOfp(preferencesData.getSimbriefSource(), null, preferencesData.getSimbriefId(), false);
      } else {
        ofp = null;
      }
      if (ofp != null) {
        log.debug("OFP has been loaded.");
        Platform.runLater(() -> {
          setOfpDataAndIcaoData(ofp);
          populateView();
          if (!ofpData.isPresent()) {
            log.debug("Could not read the OFPData from the loaded OFP.");
            Platform.runLater(() ->
                Notifications.create()
                    .title(i18N.get(VIEW_OFP_ERROR_TITLE))
                    .text(i18N.get(VIEW_OFP_ERROR_CONTENT))
                    .hideAfter(Duration.seconds(10))
                    .showError()
            );
          }
        });
      } else {
        log.debug("Could not load OFP.");
        log.debug("Returned OFP was null!");
        Platform.runLater(() ->
            Notifications.create()
                .title(i18N.get(VIEW_OFP_ERROR_TITLE))
                .text(i18N.get(VIEW_OFP_ERROR_CONTENT))
                .hideAfter(Duration.seconds(10))
                .showError()
        );
      }
    });
  }

  /**
   * Populates the OfpView with the information from {@link OfpData}.
   */
  private void populateView() {
    log.debug("Trying to populate the OFPView.");
    if (ofpData.isPresent()) {
      setViewData();
      setAlternates();
    } else {
      log.debug("Cloud not populate the OFPView because the given OFP is not present.");
      resetViewData();
    }
  }

  private void setAlternates() {
    for (alternatesCounter = 1;
         alternatesCounter <= ofpData.getOfp().getAirports().getAlternates().size();
         alternatesCounter++) {
      log.debug("Trying to set Alternate {}.", alternatesCounter);
      viewHandler.getPaneFromFxml(ViewType.OFP_TAB).ifPresent(newPane -> {
        var newTab = new Tab();
        newTab.setContent(newPane);
        newTab.setClosable(false);
        newTab.setText("Alternate " + alternatesCounter);
        tabPane.getTabs().add(newTab);
      });
      log.debug("Alternate {} has been set.", alternatesCounter);
    }
    if (!ofpData.getOfp().getAirports().getTakeOffAlternate().getIcaoCode().isEmpty()) {
      log.debug("Trying to set takeoff alternate.");
      alternatesCounter = 5;
      viewHandler.getPaneFromFxml(ViewType.OFP_TAB).ifPresent(newPane -> {
        var newTab = new Tab();
        newTab.setContent(newPane);
        newTab.setClosable(false);
        newTab.setText("Takeoff Alternate");
        tabPane.getTabs().add(newTab);
      });
      log.debug("Takeoff alternate has been set.");
    }
    if (!ofpData.getOfp().getAirports().getEnrouteAlternate().getIcaoCode().isEmpty()) {
      log.debug("Trying to set enroute alternate.");
      alternatesCounter = 6;
      viewHandler.getPaneFromFxml(ViewType.OFP_TAB).ifPresent(newPane -> {
        var newTab = new Tab();
        newTab.setContent(newPane);
        newTab.setClosable(false);
        newTab.setText("Enroute Alternate");
        tabPane.getTabs().add(newTab);
      });
      log.debug("Enroute alternate has been set.");
    }
  }

  private void setOfpDataAndIcaoData(GenericOFP ofp) {
    log.debug("Trying to populate the data singletons.");
    ofpData.setOfp(ofp);
    icaoData.setDeparture(ofp.getAirports().getOrigin().getIcaoCode());
    icaoData.setArrival(ofp.getAirports().getDestination().getIcaoCode());
    icaoData.setTakeoffAlternate(ofp.getAirports().getTakeOffAlternate().getIcaoCode());
    icaoData.setEnrouteAlternate(ofp.getAirports().getEnrouteAlternate().getIcaoCode());
    for (var i = 0; i < ofp.getAirports().getAlternates().size(); i++) {
      icaoData.getAlternates().set(i, ofp.getAirports().getAlternates().get(i).getIcaoCode());
    }
    for (var i = ofp.getAirports().getAlternates().size(); i < 4; i++) {
      icaoData.getAlternates().set(i, "");
    }
    log.debug("Data singletons have been populated.");
  }

  private void setViewData() {
    log.debug("Trying to set the data on the OFPView.");
    labelDepartureIcaoValue.setText(ofpData.getOfp().getAirports().getOrigin().getIcaoCode());
    labelDepartureNameValue.setText(ofpData.getOfp().getAirports().getOrigin().getName());
    labelDepartureTimeValue.setText(ofpData.getOfp().getTimes().getOffGroundTime()
        .format(DateTimeFormatter.ofPattern("HH:mmX")));
    labelDepartureRunwayValue.setText(ofpData.getOfp().getAirports().getOrigin().getRunway());
    labelDepartureProcedureValue.setText(ofpData.getOfp().getRouteData().getSid());
    labelArrivalIcaoValue.setText(ofpData.getOfp().getAirports().getDestination().getIcaoCode());
    labelArrivalNameValue.setText(ofpData.getOfp().getAirports().getDestination().getName());
    labelArrivalTimeValue.setText(ofpData.getOfp().getTimes().getOnGroundTime()
        .format(DateTimeFormatter.ofPattern("HH:mmX")));
    labelArrivalRunwayValue.setText(ofpData.getOfp().getAirports().getDestination().getRunway());
    labelArrivalProcedureValue.setText(ofpData.getOfp().getRouteData().getStar());
    labelRouteValue.setText(ofpData.getOfp().getRouteData().getRoute());
    labelTimeEnrouteValue.setText(i18N.get(VIEW_OFP_LABEL_TIME_ENROUTE_VALUE,
        String.valueOf(ofpData.getOfp().getTimes().getEnrouteMinutes())));
    labelAverageWCValue.setText("%s kt".formatted(String.valueOf(ofpData.getOfp().getWeather().getWindComponent())));
    log.debug("Data has been set on the OFPView");
  }

  private void resetViewData() {
    log.debug("Trying to reset the OFPView.");
    labelDepartureIcaoValue.setText("");
    labelDepartureNameValue.setText("");
    labelDepartureTimeValue.setText("");
    labelDepartureRunwayValue.setText("");
    labelDepartureProcedureValue.setText("");
    labelArrivalIcaoValue.setText("");
    labelArrivalNameValue.setText("");
    labelArrivalTimeValue.setText("");
    labelArrivalRunwayValue.setText("");
    labelArrivalProcedureValue.setText("");
    labelRouteValue.setText("");
    labelTimeEnrouteValue.setText("");
    labelAverageWCValue.setText("");
    tabPane.getTabs().clear();
    log.debug("OFPView has been reset.");
  }

  private void resetDataSingletons() {
    log.debug("Trying to reset the data singletons.");
    ofpData.reset();
    icaoData.reset();
    log.debug("Data singletons have been reset.");
  }

  /**
   * Opens a dialog in which the user can choose the path to the OFP file.
   *
   * @return {@link File} object representing the OFP file. Null when no file has been chosen.
   */
  private File showFileChooser() {
    log.debug("Trying to choose a file!");
    final var fileType = "*.xml";
    final var filter = new ExtensionFilter("XML", fileType);
    final var chooser = new FileChooser();
    chooser.getExtensionFilters().add(filter);
    return chooser.showOpenDialog(null);
  }

  /**
   * Initializes the text bindings for I18N.
   */
  @SuppressWarnings("DuplicatedCode")
  @Generated
  private void initTextBindings() {
    log.debug("Initializing the text bindings.");
    i18N.initBinding(labelDeparture.textProperty(), VIEW_OFP_LABEL_DEPARTURE);
    i18N.initBinding(labelDepartureIcao.textProperty(), VIEW_OFP_LABEL_DEPARTURE_ICAO);
    i18N.initBinding(labelDepartureName.textProperty(), VIEW_OFP_LABEL_DEPARTURE_NAME);
    i18N.initBinding(labelDepartureTime.textProperty(), VIEW_OFP_LABEL_DEPARTURE_TIME);
    i18N.initBinding(labelDepartureRunway.textProperty(), VIEW_OFP_LABEL_DEPARTURE_RUNWAY);
    i18N.initBinding(labelDepartureProcedure.textProperty(), VIEW_OFP_LABEL_DEPARTURE_PROCEDURE);
    i18N.initBinding(labelArrival.textProperty(), VIEW_OFP_LABEL_ARRIVAL);
    i18N.initBinding(labelArrivalIcao.textProperty(), VIEW_OFP_LABEL_ARRIVAL_ICAO);
    i18N.initBinding(labelArrivalName.textProperty(), VIEW_OFP_LABEL_ARRIVAL_NAME);
    i18N.initBinding(labelArrivalTime.textProperty(), VIEW_OFP_LABEL_ARRIVAL_TIME);
    i18N.initBinding(labelArrivalRunway.textProperty(), VIEW_OFP_LABEL_ARRIVAL_RUNWAY);
    i18N.initBinding(labelArrivalProcedure.textProperty(), VIEW_OFP_LABEL_ARRIVAL_PROCEDURE);
    i18N.initBinding(labelRoute.textProperty(), VIEW_OFP_LABEL_ROUTE);
    i18N.initBinding(labelTimeEnroute.textProperty(), VIEW_OFP_LABEL_TIME_ENROUTE);
    i18N.initBinding(labelAverageWC.textProperty(), VIEW_OFP_LABEL_AVERAGE_WC);
    i18N.initBinding(buttonLoad.textProperty(), VIEW_OFP_BUTTON_LOAD);
    i18N.initBinding(buttonReset.textProperty(), VIEW_OFP_BUTTON_RESET);
    log.debug("Text bindings have been initialized.");
  }

  /**
   * Shuts the executor down when the application is exiting.
   */
  @PreDestroy
  public void onExit() {
    log.debug("Stopping OfpView ExecutorService because of application shutdown.");
    executorService.shutdown();
  }
}
