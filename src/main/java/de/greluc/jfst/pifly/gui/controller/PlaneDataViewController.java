////////////////////////////////////////////////////////////////////////////////
// PiFly                                                                       /
// Copyright (C) 2021-2024 PiFly Team                                          /
//                                                                             /
// This file is part of PiFly.                                                 /
//                                                                             /
// PiFly is free software: you can redistribute it and/or modify               /
// it under the terms of the GNU General Public License as published by        /
// the Free Software Foundation, either version 3 of the License, or           /
// (at your option) any later version.                                         /
//                                                                             /
// PiFly is distributed in the hope that it will be useful,                    /
// but WITHOUT ANY WARRANTY; without even the implied warranty of              /
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               /
// GNU General Public License for more details.                                /
//                                                                             /
// You should have received a copy of the GNU General Public License           /
// along with PiFly. If not, see <http://www.gnu.org/licenses/>.               /
////////////////////////////////////////////////////////////////////////////////

package de.greluc.jfst.pifly.gui.controller;

import static de.greluc.jfst.pifly.i18n.I18NConstants.VIEW_PLANE_DATA_LABEL_ALTITUDE;
import static de.greluc.jfst.pifly.i18n.I18NConstants.VIEW_PLANE_DATA_LABEL_LATITUDE;
import static de.greluc.jfst.pifly.i18n.I18NConstants.VIEW_PLANE_DATA_LABEL_LONGITUDE;

import com.sothawo.mapjfx.Configuration;
import com.sothawo.mapjfx.Coordinate;
import com.sothawo.mapjfx.MapType;
import com.sothawo.mapjfx.MapView;
import com.sothawo.mapjfx.Projection;
import de.greluc.jfst.pifly.event.MetarResponseEvent;
import de.greluc.jfst.pifly.event.PositionUpdateEvent;
import de.greluc.jfst.pifly.i18n.I18N;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import lombok.Generated;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

/**
 * Corresponding controller for the OfpView (OfpView.fxml).
 * <p>
 * Loads a OFP and displays the most important information from it.
 *
 * @author Lucas Greuloch (greluc, lucas.greuloch@protonmail.com)
 * @version 1.2.1
 * @since 1.0.0
 */
@Component
@Slf4j
public class PlaneDataViewController {
  private final I18N i18N;
  @FXML
  public Label labelLatitude;
  @FXML
  public Label labelLatitudeValue;
  @FXML
  public Label labelLongitude;
  @FXML
  public Label labelLongitudeValue;
  @FXML
  public Label labelAltitude;
  @FXML
  public Label labelAltitudeValue;
  @FXML
  public MapView mapView;

  /**
   * Used for dependency injection.
   *
   * @param i18N Class that handles the I18N in this project.
   */
  @Contract(pure = true)
  @Autowired
  public PlaneDataViewController(@NotNull @NonNull I18N i18N) {
    this.i18N = i18N;
  }

  /**
   * Initializes the view.
   */
  @FXML
  @Generated
  void initialize() {
    initTextBindings();
    initMap();
  }

  /**
   * Notifies the view that a new METAR has been received.
   *
   * @param event Contains the newly received METAR.
   */
  @EventListener(MetarResponseEvent.class)
  public void onPositionUpdateEvent(@NotNull @NonNull PositionUpdateEvent event) {
    labelLatitudeValue.setText(Double.toString(event.getLatitude()));
    labelLongitudeValue.setText(Double.toString(event.getLongitude()));
    labelAltitudeValue.setText(Long.toString(event.getAltitude()));
  }

  /**
   * Called after the FXML is loaded and all objects are created. This is not called initialize anymore,
   * because we need to pass on the projection before initializing.
   */
  public void initMap() {
    final Projection projection = Projection.WEB_MERCATOR;
    mapView.setCenter(new Coordinate(48.993284, 8.402186));
    mapView.setMapType(MapType.OSM);
    mapView.initialize(Configuration.builder()
        .projection(projection)
        .showZoomControls(false)
        .build());
  }

  /**
   * Initializes the text bindings for I18N.
   */
  @SuppressWarnings("DuplicatedCode")
  @Generated
  private void initTextBindings() {
    log.debug("Initializing the text bindings.");
    i18N.initBinding(labelLatitude.textProperty(), VIEW_PLANE_DATA_LABEL_LATITUDE);
    i18N.initBinding(labelLongitude.textProperty(), VIEW_PLANE_DATA_LABEL_LONGITUDE);
    i18N.initBinding(labelAltitude.textProperty(), VIEW_PLANE_DATA_LABEL_ALTITUDE);
    log.debug("Text bindings have been initialized.");
  }
}
