////////////////////////////////////////////////////////////////////////////////
// PiFly                                                                       /
// Copyright (C) 2021-2024 PiFly Team                                          /
//                                                                             /
// This file is part of PiFly.                                                 /
//                                                                             /
// PiFly is free software: you can redistribute it and/or modify               /
// it under the terms of the GNU General Public License as published by        /
// the Free Software Foundation, either version 3 of the License, or           /
// (at your option) any later version.                                         /
//                                                                             /
// PiFly is distributed in the hope that it will be useful,                    /
// but WITHOUT ANY WARRANTY; without even the implied warranty of              /
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               /
// GNU General Public License for more details.                                /
//                                                                             /
// You should have received a copy of the GNU General Public License           /
// along with PiFly. If not, see <http://www.gnu.org/licenses/>.               /
////////////////////////////////////////////////////////////////////////////////

package de.greluc.jfst.pifly.gui.controller;

import de.greluc.jfst.pifly.event.LocaleChangeEvent;
import de.greluc.jfst.pifly.event.ViewChangeEvent;
import de.greluc.jfst.pifly.gui.ViewType;
import de.greluc.jfst.pifly.i18n.I18N;
import de.greluc.jfst.pifly.i18n.I18NConstants;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.util.Duration;
import javafx.util.StringConverter;
import lombok.Generated;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

/**
 * Controls the {@code SelectionView} of the application.
 * <p>
 * Changes the shown view depending on the users' selection.
 *
 * @author Lucas Greuloch (greluc, lucas.greuloch@protonmail.com)
 * @since 1.0.0
 * @version 1.2.1
 */
@Component
@Slf4j
public class SelectionViewController {

  private final ApplicationContext applicationContext;
  private final I18N i18N;
  @FXML
  private Label labelTimeZulu;
  @FXML
  private Label timeZulu;
  @FXML
  private Label labelTimeLocal;
  @FXML
  private Label timeLocal;
  @FXML
  private ComboBox<ViewType> viewSelection;

  /**
   * Used for dependency injection.
   *at provides helper methods for view management.
   * @param applicationContext Spring Boot {@link ApplicationContext} used for event.
   * @param i18N Class that handles the I18N in this project.
   */
  @Contract(pure = true)
  @Autowired
  @Generated
  public SelectionViewController(@NotNull @NonNull ApplicationContext applicationContext, @NotNull @NonNull I18N i18N) {
    this.applicationContext = applicationContext;
    this.i18N = i18N;
  }

  /**
   * Initializes the view.
   */
  @FXML
  @Generated
  void initialize() {
    initTextBindings();
    initClock();
    viewSelection.setItems(
        FXCollections
            .observableArrayList(ViewType.WELCOME, ViewType.METAR, ViewType.TAF, ViewType.OFP, ViewType.PLANE_DATA));
    viewSelection.getSelectionModel().select(ViewType.WELCOME);
    viewSelection.setConverter(new StringConverter<>() {
      @Override
      @NotNull
      @Generated
      public String toString(ViewType type) {
        if (type == null) {
          return "";
        }
        return switch (type) {
          case METAR -> i18N.get(I18NConstants.VIEW_METAR);
          case TAF -> i18N.get(I18NConstants.VIEW_TAF);
          case ICAO -> i18N.get(I18NConstants.VIEW_ICAO);
          case OFP -> i18N.get(I18NConstants.VIEW_OFP);
          case PLANE_DATA -> i18N.get(I18NConstants.VIEW_PLANE_DATA);
          case SETTINGS -> i18N.get(I18NConstants.VIEW_SETTINGS);
          case SELECTION -> i18N.get(I18NConstants.VIEW_SELECTION);
          default -> i18N.get(I18NConstants.VIEW_WELCOME);
        };
      }

      @Override
      @Generated
      public ViewType fromString(String string) {
        return null;
      }
    });
  }

  /**
   * Initializes the text bindings in this view.
   */
  @Generated
  private void initTextBindings() {
    i18N.initBinding(labelTimeZulu.textProperty(), I18NConstants.VIEW_SELECTION_LABEL_TIME_ZULU);
    i18N.initBinding(labelTimeLocal.textProperty(), I18NConstants.VIEW_SELECTION_LABEL_TIME_LOCAL);
  }

  /**
   * Initializes the two clocks. One displays the time in ZULU and the other in the locale time
   * zone.
   */
  @Generated
  private void initClock() {
    var clockZulu = new Timeline(new KeyFrame(Duration.ZERO, e -> {
      var formatter = DateTimeFormatter.ofPattern("HH:mm:ss");
      timeZulu.setText(ZonedDateTime.now(ZoneId.of("GMT+0")).format(formatter));
    }), new KeyFrame(Duration.seconds(1)));
    clockZulu.setCycleCount(Animation.INDEFINITE);
    clockZulu.play();

    var clockLocal = new Timeline(new KeyFrame(Duration.ZERO, e -> {
      var formatter = DateTimeFormatter.ofPattern("HH:mm:ss");
      timeLocal.setText(ZonedDateTime.now(ZoneId.systemDefault()).format(formatter));
    }), new KeyFrame(Duration.seconds(1)));
    clockLocal.setCycleCount(Animation.INDEFINITE);
    clockLocal.play();
  }

  /**
   * Is called when a view is selected in {@code viewSelection}. Changes the child view to the
   * selected one.
   */
  @FXML
  @Generated
  private void viewSelected() {
    switch (viewSelection.getSelectionModel().getSelectedItem()) {
      case METAR -> {
        log.debug("MetarView has been selected");
        applicationContext.publishEvent(new ViewChangeEvent(this, ViewType.METAR));
      }
      case TAF -> {
        log.debug("TafView has been selected");
        applicationContext.publishEvent(new ViewChangeEvent(this, ViewType.TAF));
      }
      case OFP -> {
        log.debug("OfpView has been selected");
        applicationContext.publishEvent(new ViewChangeEvent(this, ViewType.OFP));
      }
      case PLANE_DATA -> {
        log.debug("PlaneDataView has been selected");
        applicationContext.publishEvent(new ViewChangeEvent(this, ViewType.PLANE_DATA));
      }
      default -> {
        log.debug("WelcomeView has been selected");
        applicationContext.publishEvent(new ViewChangeEvent(this, ViewType.WELCOME));
      }
    }
  }

  /**
   * Forces the viewSelection to reload the visible texts when the {@code Locale} gets updated.
   */
  @EventListener(LocaleChangeEvent.class)
  @Generated
  public void onLocaleChangeEvent() {
    Platform.runLater(() -> {
      EventHandler<ActionEvent> eventHandler = viewSelection.getOnAction();
      viewSelection.setOnAction(null);
      viewSelection.setItems(
          FXCollections
              .observableArrayList(ViewType.WELCOME, ViewType.METAR, ViewType.TAF, ViewType.OFP, ViewType.PLANE_DATA));
      viewSelection.setOnAction(eventHandler);
    });
  }
}
