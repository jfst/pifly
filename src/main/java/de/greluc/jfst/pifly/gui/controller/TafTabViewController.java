////////////////////////////////////////////////////////////////////////////////
// PiFly                                                                       /
// Copyright (C) 2021-2024 PiFly Team                                          /
//                                                                             /
// This file is part of PiFly.                                                 /
//                                                                             /
// PiFly is free software: you can redistribute it and/or modify               /
// it under the terms of the GNU General Public License as published by        /
// the Free Software Foundation, either version 3 of the License, or           /
// (at your option) any later version.                                         /
//                                                                             /
// PiFly is distributed in the hope that it will be useful,                    /
// but WITHOUT ANY WARRANTY; without even the implied warranty of              /
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               /
// GNU General Public License for more details.                                /
//                                                                             /
// You should have received a copy of the GNU General Public License           /
// along with PiFly. If not, see <http://www.gnu.org/licenses/>.               /
////////////////////////////////////////////////////////////////////////////////

package de.greluc.jfst.pifly.gui.controller;

import de.greluc.jfst.pifly.data.TafData;
import de.greluc.jfst.pifly.i18n.I18N;
import de.greluc.jfst.pifly.i18n.I18NConstants;
import io.github.mivek.enums.Phenomenon;
import io.github.mivek.enums.WeatherChangeType;
import io.github.mivek.model.Metar;
import io.github.mivek.model.TAF;
import io.github.mivek.model.trend.validity.BeginningValidity;
import io.github.mivek.model.trend.validity.Validity;
import javafx.beans.property.StringProperty;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import lombok.Generated;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * Corresponding controller for the TafView (TafTabView.fxml).
 * <p>
 * Shows the IcaoView to get an ICAO code and displays the retrieved and decoded TAF to the user.
 *
 * @author Lucas Greuloch (greluc, lucas.greuloch@protonmail.com)
 * @since 1.0.0
 * @version 1.2.1
 */
@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@Slf4j
public class TafTabViewController {

  private final I18N i18N;
  private final TafViewController tafViewController;
  @FXML
  private Label labelValidity;
  @FXML
  private Label labelValidityValue;
  @FXML
  private Label labelWind;
  @FXML
  private Label labelWindValue;
  @FXML
  private Label labelCloudCoverage;
  @FXML
  private Label labelCloudCoverageValue;
  @FXML
  private Label labelCloudHeight;
  @FXML
  private Label labelCloudHeightValue;
  @FXML
  private Label labelVisibility;
  @FXML
  private Label labelVisibilityValue;
  @FXML
  private Label labelType;
  @FXML
  private Label labelTypeValue;
  @FXML
  private Label labelPhenomena;
  @FXML
  private Label labelPhenomenaValue;

  /**
   * Used for dependency injection.
   *
   * @param i18N Class that handles the I18N in this project.
   * @param tafViewController Controller that delivers the TAF trend data.
   */
  @Contract(pure = true)
  @Autowired
  @Generated
  public TafTabViewController(@NotNull @NonNull I18N i18N,
      @NotNull @NonNull TafViewController tafViewController) {
    this.i18N = i18N;
    this.tafViewController = tafViewController;
  }

  /**
   * Initializes the view. E.g. sets the IcaoView as a child and inits the text bindings for I18N.
   */
  @FXML
  @Generated
  public void initialize() {
    initTextBindings();
    setTrend(tafViewController.getTafDates().get(tafViewController.getTrendCounter()));
  }

  /**
   * Initializes the text bindings for I18N.
   */
  @Generated
  private void initTextBindings() {
    i18N.initBinding(labelValidity.textProperty(), I18NConstants.VIEW_TAF_VALIDITY);
    i18N.initBinding(labelWind.textProperty(), I18NConstants.VIEW_METAR_WIND);
    i18N.initBinding(labelCloudCoverage.textProperty(), I18NConstants.VIEW_METAR_CLOUD_COVERAGE);
    i18N.initBinding(labelCloudHeight.textProperty(), I18NConstants.VIEW_METAR_CLOUD_HEIGHT);
    i18N.initBinding(labelVisibility.textProperty(), I18NConstants.VIEW_METAR_VISIBILITY);
    i18N.initBinding(labelType.textProperty(), I18NConstants.VIEW_TAF_TYPE);
    i18N.initBinding(labelPhenomena.textProperty(), I18NConstants.VIEW_TAF_PHENOMENA);
  }

  /**
   * Sets the corresponding TAF data for this tab.
   *
   * @param data Contains the newly received TAF.
   */
  void setTrend(TafData data) {
    if (data == null) {
      return;
    }
    if (data.visibility() != null) {
      labelVisibilityValue.setText(data.visibility().getMainVisibility());
    }
    setType(data, labelTypeValue.textProperty());
    setValidity(data, labelValidityValue.textProperty());
    setWind(data, labelWindValue.textProperty());
    setCloudCoverage(data, labelCloudCoverageValue.textProperty());
    setCloudsHeight(data, labelCloudHeightValue.textProperty());
    setPhenomena(data, labelPhenomenaValue.textProperty());
  }

  /**
   * Reads the phenomena from the {@link TAF}.
   *
   * @param data Contains the type.
   * @param stringProperty The aim of the operation.
   */
   void setPhenomena(@NotNull @NonNull TafData data, @NotNull @NonNull StringProperty stringProperty) {
     if (data.conditions() == null || data.conditions().isEmpty()) {
       stringProperty.set("");
     } else {
       StringBuilder text = new StringBuilder();
       if (data.conditions().get(0).getIntensity() != null) {
         text.append(data.conditions().get(0).getIntensity().toString()).append(" ");
       }
       if (data.conditions().get(0).getDescriptive() != null) {
         text.append(data.conditions().get(0).getDescriptive().toString()).append(" ");
       }
       if (!data.conditions().get(0).getPhenomenons().isEmpty()
           && data.conditions().get(0).getPhenomenons().get(0) != null) {
         for (Phenomenon phenomenon: data.conditions().get(0).getPhenomenons()) {
           text.append(phenomenon.toString()).append(" ");
         }
       }
       stringProperty.set(text.toString());
     }
  }

  /**
   * Reads the type of the trend from the {@link TAF}.
   *
   * @param data Contains the type.
   * @param stringProperty The aim of the operation.
   */
  void setType(@NotNull @NonNull TafData data, @NotNull @NonNull StringProperty stringProperty) {
    if (data.type() == null) {
      stringProperty.set("");
    } else {
      String type = switch (data.type()) {
        case BECMG -> "BECMG";
        case PROB -> "PROB";
        case TEMPO -> "TEMPO";
        case FM -> "FM";
        case INTER -> "INTER";
      };
      stringProperty.set(type);
    }
  }

  /**
   * Reads the validity from the {@link TAF} in feet AGL.
   *
   * @param data Contains the validity data.
   * @param stringProperty The aim of the operation.
   */
  void setValidity(@NotNull @NonNull TafData data,
      @NotNull @NonNull StringProperty stringProperty) {
    stringProperty.unbind();
    if (data.type() != null && data.type() == WeatherChangeType.FM) {
      var validity = (BeginningValidity) data.validity();
      i18N.initBinding(stringProperty, I18NConstants.VIEW_TAF_VALIDITY_VALUE_BEGINNING,
          validity.getStartDay(),
          String.format("%02d", validity.getStartHour()));
    } else {
      var validity = (Validity) data.validity();
      i18N.initBinding(stringProperty, I18NConstants.VIEW_TAF_VALIDITY_VALUE,
          validity.getStartDay(),
          String.format("%02d", validity.getStartHour()), validity.getEndDay(),
          String.format("%02d", validity.getEndHour()));
    }
  }

  /**
   * Reads the lower border height of the clouds from the {@link TAF} in feet AGL.
   *
   * @param data Contains the height of the clouds in feet.
   * @param stringProperty The aim of the operation.
   */
  void setCloudsHeight(@NotNull @NonNull TafData data,
      @NotNull @NonNull StringProperty stringProperty) {
    stringProperty.unbind();
    if (data.clouds().isEmpty()) {
      stringProperty.set("");
      return;
    }
    i18N.initBinding(stringProperty, I18NConstants.VIEW_METAR_CLOUD_UNIT_FEET,
        data.clouds().get(0).getHeight());
  }

  /**
   * Reads the coverage of the clouds from the {@link TAF}. Shows the code word (e.g. FEW) and the
   * value in eighths.
   *
   * @param data Contains the coverage of the clouds.
   * @param stringProperty The aim of the operation.
   */
  void setCloudCoverage(@NotNull @NonNull TafData data,
      @NotNull @NonNull StringProperty stringProperty) {
    stringProperty.unbind();
    if (data.clouds().isEmpty()) {
      stringProperty.set("");
      return;
    }
    switch (data.clouds().get(0).getQuantity()) {
      case OVC -> i18N.initBinding(stringProperty, I18NConstants.VIEW_METAR_CLOUD_COVERAGE_OVC);
      case BKN -> i18N.initBinding(stringProperty, I18NConstants.VIEW_METAR_CLOUD_COVERAGE_BKN);
      case SCT -> i18N.initBinding(stringProperty, I18NConstants.VIEW_METAR_CLOUD_COVERAGE_SCT);
      case FEW -> i18N.initBinding(stringProperty, I18NConstants.VIEW_METAR_CLOUD_COVERAGE_FEW);
      case NSC -> i18N.initBinding(stringProperty, I18NConstants.VIEW_METAR_CLOUD_COVERAGE_NSC);
      default -> i18N.initBinding(stringProperty, I18NConstants.VIEW_METAR_CLOUD_COVERAGE_SKC);
    }
  }

  /**
   * Reads the wind data from the {@link Metar}.
   *
   * @param data Contains the wind data.
   * @param stringProperty The aim of the operation.
   */
  private void setWind(@NotNull @NonNull TafData data,
      @NotNull @NonNull StringProperty stringProperty) {
    stringProperty.set(setWindDirection(data) + " " + setWindSpeed(data));
  }

  /**
   * Reads the direction of the wind from the {@link Metar}.
   *
   * @param data Contains the wind information.
   */
  private String setWindDirection(@NotNull @NonNull TafData data) {
    if (data.wind() != null) {
      if (data.wind().getDirectionDegrees() == null) {
        return i18N.get(I18NConstants.VIEW_METAR_WIND_DIRECTION_VARYING);
      } else {
        int windDirection = data.wind().getDirectionDegrees();
        int windVariationMin = data.wind().getMinVariation() != null ? data.wind().getMinVariation() : 0;
        int windVariationMax = data.wind().getMaxVariation() != null ? data.wind().getMaxVariation() : 0;
        if (windVariationMin != windVariationMax) {
          return i18N.get(I18NConstants.VIEW_METAR_WIND_DIRECTION_RANGE, windDirection,
              windVariationMin, windVariationMax);
        } else {
          return i18N.get(I18NConstants.VIEW_METAR_WIND_DIRECTION_FIXED, windDirection);
        }
      }
    }
    return "";
  }

  /**
   * Reads the speed of the wind from the {@link Metar}.
   *
   * @param data Contains the wind information.
   */
  String setWindSpeed(@NotNull @NonNull TafData data) {
    if (data.wind() != null) {
      if (data.wind().getGust() != null && data.wind().getGust() > 0) {
        return i18N.get(
            I18NConstants.VIEW_METAR_WIND_SPEED_GUSTS, data.wind().getSpeed(),
            data.wind().getUnit(),
            data.wind().getGust());
      } else {
        return i18N.get(I18NConstants.VIEW_METAR_WIND_SPEED, data.wind().getSpeed(),
            data.wind().getUnit());
      }
    }
    return "";
  }
}
