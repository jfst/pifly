////////////////////////////////////////////////////////////////////////////////
// PiFly                                                                       /
// Copyright (C) 2021-2024 PiFly Team                                          /
//                                                                             /
// This file is part of PiFly.                                                 /
//                                                                             /
// PiFly is free software: you can redistribute it and/or modify               /
// it under the terms of the GNU General Public License as published by        /
// the Free Software Foundation, either version 3 of the License, or           /
// (at your option) any later version.                                         /
//                                                                             /
// PiFly is distributed in the hope that it will be useful,                    /
// but WITHOUT ANY WARRANTY; without even the implied warranty of              /
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               /
// GNU General Public License for more details.                                /
//                                                                             /
// You should have received a copy of the GNU General Public License           /
// along with PiFly. If not, see <http://www.gnu.org/licenses/>.               /
////////////////////////////////////////////////////////////////////////////////

package de.greluc.jfst.pifly.gui.controller;

import static de.greluc.jfst.pifly.i18n.I18NConstants.VIEW_METAR_AIRPORT_NAME;
import static de.greluc.jfst.pifly.i18n.I18NConstants.VIEW_METAR_TIME;
import static de.greluc.jfst.pifly.i18n.I18NConstants.VIEW_TAF_AREA_PROMPT;

import de.greluc.jfst.pifly.data.IcaoData;
import de.greluc.jfst.pifly.data.TafData;
import de.greluc.jfst.pifly.event.ActiveViewEvent;
import de.greluc.jfst.pifly.event.IcaoCodeChangeEvent;
import de.greluc.jfst.pifly.event.StartWeatherUpdateEvent;
import de.greluc.jfst.pifly.event.StopWeatherUpdateEvent;
import de.greluc.jfst.pifly.event.TafResponseEvent;
import de.greluc.jfst.pifly.gui.ViewHandler;
import de.greluc.jfst.pifly.gui.ViewType;
import de.greluc.jfst.pifly.i18n.I18N;
import io.github.mivek.enums.WeatherChangeType;
import io.github.mivek.model.trend.FMTafTrend;
import io.github.mivek.model.trend.TafProbTrend;
import io.github.mivek.model.trend.TafTrend;
import java.time.format.DateTimeFormatter;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TextArea;
import javafx.scene.layout.GridPane;
import lombok.Generated;
import lombok.Getter;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

/**
 * Corresponding controller for the TafView (TafView.fxml).
 * <p>
 * Shows the IcaoView to get an ICAO code and displays the retrieved and decoded TAF to the user.
 *
 * @author Lucas Greuloch (greluc, lucas.greuloch@protonmail.com)
 * @since 1.0.0
 * @version 1.2.1
 */
@Component
@Slf4j
public class TafViewController {

  private final I18N i18N;
  private final ViewHandler viewHandler;
  private final ApplicationContext applicationContext;
  private final IcaoData icaoData;
  @Getter
  private final List<TafData> tafDates = new LinkedList<>();
  private boolean isActive;
  @Getter
  private int trendCounter;
  @FXML
  private GridPane pane;
  @FXML
  private TextArea textAreaTafValue;
  @FXML
  private Label labelAirportName;
  @FXML
  private Label labelAirportNameValue;
  @FXML
  private Label labelIssueTime;
  @FXML
  private Label labelIssueTimeValue;
  @FXML
  private TabPane tabPane;

  /**
   * Used for dependency injection.
   *
   * @param i18N Class that handles the I18N in this project.
   * @param viewHandler Required to set the View.
   * @param applicationContext Spring Boot {@link ApplicationContext} used for event
   * @param icaoData POJO that stores the different ICAO codes.
   */
  @Contract(pure = true)
  @Autowired
  @Generated
  public TafViewController(@NotNull @NonNull I18N i18N,
      @NotNull @NonNull ViewHandler viewHandler,
      @NotNull @NonNull ApplicationContext applicationContext,
      @NotNull @NonNull IcaoData icaoData) {
    this.i18N = i18N;
    this.viewHandler = viewHandler;
    this.applicationContext = applicationContext;
    this.icaoData = icaoData;
  }

  /**
   * Initializes the view. E.g. sets the IcaoView as a child and inits the text bindings for I18N.
   */
  @FXML
  @Generated
  public void initialize() {
    viewHandler.getPaneFromFxml(ViewType.ICAO).ifPresent(childPane -> pane.add(childPane, 0, 0, 4, 1));
    initTextBindings();
  }

  /**
   * Initializes the text bindings for I18N.
   */
  @Generated
  private void initTextBindings() {
    i18N.initBinding(textAreaTafValue.promptTextProperty(), VIEW_TAF_AREA_PROMPT);
    i18N.initBinding(labelAirportName.textProperty(), VIEW_METAR_AIRPORT_NAME);
    i18N.initBinding(labelIssueTime.textProperty(), VIEW_METAR_TIME);
  }

  /**
   * Notifies the view that the user has selected a new ICAO code.
   *
   * @param ignoredEvent Contains the new ICAO code.
   */
  @EventListener(IcaoCodeChangeEvent.class)
  public void onIcaoCodeChangeEvent(@NotNull @NonNull IcaoCodeChangeEvent ignoredEvent) {
    if (isActive) {
      applicationContext.publishEvent(new StartWeatherUpdateEvent(this, icaoData.getManual(),false));
    }
  }

  /**
   * Notifies the view that a new view is currently shown in the application.
   * <p>
   * Used to determine whether the view needs to query the TAF or not.
   *
   * @param event Contains the type of the currently shown view.
   */
  @EventListener(ActiveViewEvent.class)
  public void onActiveViewEvent(@NotNull @NonNull ActiveViewEvent event) {
    isActive = event.getType().equals(ViewType.TAF);
    if (!isActive) {
      applicationContext.publishEvent(new StopWeatherUpdateEvent(this));
    }
  }

  /**
   * Notifies the view that a new TAF has been received.
   *
   * @param event Contains the newly received TAF.
   */
  @EventListener(TafResponseEvent.class)
  public void onTafResponseEvent(@NotNull @NonNull TafResponseEvent event) {
    Platform.runLater(() -> {
      tabPane.getTabs().clear();
      textAreaTafValue.setText(event.getTaf().getMessage());
      labelAirportNameValue.setText(event.getTaf().getAirport().getName().replace(" Airport", ""));
      labelIssueTimeValue.textProperty().set(String.format("%sZ", event.getTaf().getTime().format(
          DateTimeFormatter.ofPattern("HH:mm"))));
      extractTafDates(event);
      for (trendCounter = 0; trendCounter < tafDates.size(); trendCounter++) {
        viewHandler.getPaneFromFxml(ViewType.TAF_TAB).ifPresent(newPane -> {
          var newTab = new Tab();
          newTab.setContent(newPane);
          newTab.setClosable(false);
          if (trendCounter == 0) {
            newTab.setText("TAF");
          } else {
            String type = switch (tafDates.get(trendCounter).type()) {
              case BECMG -> "BECMG";
              case PROB -> "PROB";
              case TEMPO -> "TEMPO";
              case FM -> "FM";
              case INTER -> "INTER";
            };
            newTab.setText(String
                .format("%s %02d%02d", type, tafDates.get(trendCounter).validity().getStartDay(),
                    tafDates.get(trendCounter).validity().getStartHour()));
          }
          tabPane.getTabs().add(newTab);
        });
      }
    });
  }

  /**
   * Reads all the trends of the TAF contained in the {@link TafResponseEvent} and stores them in
   * tafDates.
   *
   * @param event Event containing the newly received TAF.
   */
  private void extractTafDates(@NotNull TafResponseEvent event) {
    tafDates.clear();
    for (FMTafTrend fmTafTrend : event.getTaf().getFMs()) {
      tafDates.add(new TafData(WeatherChangeType.FM,
          fmTafTrend.getValidity(), fmTafTrend.getVisibility(), fmTafTrend.getClouds(),
          fmTafTrend.getWind(), fmTafTrend.getWeatherConditions()));
    }
    for (TafTrend becmgTafTrend : event.getTaf().getBECMGs()) {
      tafDates.add(new TafData(WeatherChangeType.BECMG,
          becmgTafTrend.getValidity(), becmgTafTrend.getVisibility(), becmgTafTrend.getClouds(),
          becmgTafTrend.getWind(), becmgTafTrend.getWeatherConditions()));
    }
    for (TafTrend interTafTrend : event.getTaf().getInters()) {
      tafDates.add(new TafData(WeatherChangeType.INTER,
          interTafTrend.getValidity(), interTafTrend.getVisibility(), interTafTrend.getClouds(),
          interTafTrend.getWind(), interTafTrend.getWeatherConditions()));
    }
    for (TafProbTrend probTafTrend : event.getTaf().getProbs()) {
      tafDates.add(new TafData(WeatherChangeType.PROB,
          probTafTrend.getValidity(), probTafTrend.getVisibility(), probTafTrend.getClouds(),
          probTafTrend.getWind(), probTafTrend.getWeatherConditions()));
    }
    for (TafProbTrend tempoTafTrend : event.getTaf().getTempos()) {
      tafDates.add(new TafData(WeatherChangeType.TEMPO,
          tempoTafTrend.getValidity(), tempoTafTrend.getVisibility(), tempoTafTrend.getClouds(),
          tempoTafTrend.getWind(), tempoTafTrend.getWeatherConditions()));
    }
    tafDates.sort(Comparator.comparingInt((TafData tafData) -> tafData.validity().getStartDay())
        .thenComparingInt(tafData -> tafData.validity().getStartHour()));
    tafDates.add(0, new TafData(null, event.getTaf().getValidity(),
        event.getTaf().getVisibility(),
        event.getTaf().getClouds(),
        event.getTaf().getWind(), event.getTaf().getWeatherConditions()));
  }
}
