////////////////////////////////////////////////////////////////////////////////
// PiFly                                                                       /
// Copyright (C) 2021-2024 PiFly Team                                          /
//                                                                             /
// This file is part of PiFly.                                                 /
//                                                                             /
// PiFly is free software: you can redistribute it and/or modify               /
// it under the terms of the GNU General Public License as published by        /
// the Free Software Foundation, either version 3 of the License, or           /
// (at your option) any later version.                                         /
//                                                                             /
// PiFly is distributed in the hope that it will be useful,                    /
// but WITHOUT ANY WARRANTY; without even the implied warranty of              /
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               /
// GNU General Public License for more details.                                /
//                                                                             /
// You should have received a copy of the GNU General Public License           /
// along with PiFly. If not, see <http://www.gnu.org/licenses/>.               /
////////////////////////////////////////////////////////////////////////////////

package de.greluc.jfst.pifly.i18n;

import de.greluc.jfst.pifly.CommonConstants;
import lombok.Generated;
import org.jetbrains.annotations.Contract;

/**
 * Holds the keys used in the messages bundle.
 *
 * @author Lucas Greuloch (greluc, lucas.greuloch@protonmail.com)
 * @since 1.0.0
 * @version 1.2.1
 */
public class I18NConstants {

  // I18N
  public static final String NOTIFICATION_TAF_EXCEPTION_PARSE_TITLE = "notification.taf.exception.parse.title";
  public static final String NOTIFICATION_TAF_EXCEPTION_PARSE_CONTENT = "notification.taf.exception.parse.content";
  public static final String NOTIFICATION_TAF_EXCEPTION_IO_TITLE = "notification.taf.exception.io.title";
  public static final String NOTIFICATION_TAF_EXCEPTION_IO_CONTENT = "notification.taf.exception.io.content";
  public static final String NOTIFICATION_METAR_EXCEPTION_PARSE_TITLE = "notification.metar.exception.parse.title";
  public static final String NOTIFICATION_METAR_EXCEPTION_PARSE_CONTENT = "notification.metar.exception.parse.content";
  public static final String NOTIFICATION_METAR_EXCEPTION_IO_TITLE = "notification.metar.exception.io.title";
  public static final String NOTIFICATION_METAR_EXCEPTION_IO_CONTENT = "notification.metar.exception.io.content";
  public static final String NOTIFICATION_OFP_EXCEPTION_FILE_TITLE = "notification.ofp.exception.file.title";
  public static final String NOTIFICATION_OFP_EXCEPTION_FILE_CONTENT = "notification.ofp.exception.file.content";
  public static final String NOTIFICATION_OFP_EXCEPTION_IO_TITLE = "notification.ofp.exception.io.title";
  public static final String NOTIFICATION_OFP_EXCEPTION_IO_CONTENT = "notification.ofp.exception.io.content";
  public static final String ALERT_ERROR_HEADER = "alert.error.header";
  public static final String ALERT_ERROR_CONTENT_GENERAL = "alert.error.content.general";
  public static final String NOTIFICATION_DECLINATION_ERROR_TITLE = "view.declination.error.title";
  public static final String NOTIFICATION_DECLINATION_ERROR_CONTENT = "view.declination.error.content";
  public static final String VIEW_ICAO_TEXTFIELD_PROMPT = "view.icao.textfield.prompt";
  public static final String VIEW_ICAO_TEXTFIELD_TOOLTIP = "view.icao.textfield.tooltip";
  public static final String VIEW_ICAO_LABEL_AIRPORT = "view.icao.label.airport";
  public static final String VIEW_ICAO_ERROR_TITLE = "view.icao.error.title";
  public static final String VIEW_ICAO_ERROR_CONTENT = "view.icao.error.content";
  public static final String VIEW_ICAO = "view.icao";
  public static final String VIEW_METAR = "view.metar";
  public static final String VIEW_METAR_AIRPORT_NAME = "view.metar.airport.name";
  public static final String VIEW_METAR_TIME = "view.metar.time";
  public static final String VIEW_METAR_ALTIMETER = "view.metar.altimeter";
  public static final String VIEW_METAR_WIND = "view.metar.wind";
  public static final String VIEW_METAR_WIND_SPEED = "view.metar.wind.speed";
  public static final String VIEW_METAR_WIND_SPEED_GUSTS = "view.metar.wind.speed.gusts";
  public static final String VIEW_METAR_WIND_DIRECTION_VARYING = "view.metar.wind.direction.varying";
  public static final String VIEW_METAR_WIND_DIRECTION_FIXED = "view.metar.wind.direction.fixed";
  public static final String VIEW_METAR_WIND_DIRECTION_RANGE = "view.metar.wind.direction.range";
  public static final String VIEW_METAR_TEMPERATURE = "view.metar.temperature";
  public static final String VIEW_METAR_CLOUD_COVERAGE = "view.metar.cloud.coverage";
  public static final String VIEW_METAR_CLOUD_COVERAGE_NSC = "view.metar.cloud.coverage.nsc";
  public static final String VIEW_METAR_CLOUD_COVERAGE_SKC = "view.metar.cloud.coverage.skc";
  public static final String VIEW_METAR_CLOUD_COVERAGE_FEW = "view.metar.cloud.coverage.few";
  public static final String VIEW_METAR_CLOUD_COVERAGE_SCT = "view.metar.cloud.coverage.sct";
  public static final String VIEW_METAR_CLOUD_COVERAGE_BKN = "view.metar.cloud.coverage.bkn";
  public static final String VIEW_METAR_CLOUD_COVERAGE_OVC = "view.metar.cloud.coverage.ovc";
  public static final String VIEW_METAR_CLOUD_COVERAGE_CAVOK = "view.metar.cloud.coverage.cavok";
  public static final String VIEW_METAR_CLOUD_HEIGHT = "view.metar.cloud.height";
  public static final String VIEW_METAR_VISIBILITY = "view.metar.visibility";
  public static final String VIEW_METAR_PHENOMENON = "view.metar.phenomenon";
  public static final String VIEW_METAR_CLOUD_UNIT_FEET = "view.metar.cloud.height.unit.feet";
  public static final String VIEW_METAR_CLOUD_UNIT_METER = "view.metar.cloud.height.unit.meter";
  public static final String VIEW_METAR_DEWPOINT = "view.metar.dewpoint";
  public static final String VIEW_METAR_AREA_PROMPT = "view.metar.area.prompt";
  public static final String VIEW_METAR_ONLINE = "view.metar.online";
  public static final String VIEW_TAF = "view.taf";
  public static final String VIEW_TAF_TYPE = "view.taf.type";
  public static final String VIEW_TAF_PHENOMENA = "view.taf.phenomena";
  public static final String VIEW_TAF_VALIDITY = "view.taf.validity";
  public static final String VIEW_TAF_VALIDITY_VALUE = "view.taf.validity.value";
  public static final String VIEW_TAF_VALIDITY_VALUE_BEGINNING = "view.taf.validity.value.beginning";
  public static final String VIEW_TAF_AREA_PROMPT = "view.taf.area.prompt";
  public static final String VIEW_SETTINGS = "view.settings";
  public static final String VIEW_SELECTION = "view.selection";
  public static final String VIEW_WELCOME = "view.welcome";
  public static final String VIEW_MAIN_TITLE = "view.main.title";
  public static final String VIEW_MAIN_MENU_FILE = "view.main.menu.file";
  public static final String VIEW_MAIN_MENU_FILE_SETTINGS = "view.main.menu.file.settings";
  public static final String VIEW_MAIN_MENU_FILE_QUIT = "view.main.menu.file.quit";
  public static final String VIEW_MAIN_MENU_OFP = "view.main.menu.ofp";
  public static final String VIEW_MAIN_MENU_OFP_SIMBRIEF = "view.main.menu.ofp.simbrief";
  public static final String VIEW_MAIN_MENU_OFP_SIMBRIEF_LOCAL = "view.main.menu.ofp.simbrief.local";
  public static final String VIEW_MAIN_MENU_OFP_SIMBRIEF_ONLINE = "view.main.menu.ofp.simbrief.online";
  public static final String VIEW_MAIN_MENU_HELP = "view.main.menu.help";
  public static final String VIEW_MAIN_MENU_HELP_ABOUT = "view.main.menu.help.about";
  public static final String VIEW_OFP = "view.ofp";
  public static final String VIEW_OFP_BUTTON_LOAD = "view.ofp.button.load";
  public static final String VIEW_OFP_BUTTON_RESET = "view.ofp.button.reset";
  public static final String VIEW_OFP_ERROR_TITLE = "view.ofp.error.title";
  public static final String VIEW_OFP_ERROR_CONTENT = "view.ofp.error.content";
  public static final String VIEW_OFP_LABEL_DEPARTURE = "view.ofp.label.departure";
  public static final String VIEW_OFP_LABEL_DEPARTURE_ICAO = "view.ofp.label.departure.icao";
  public static final String VIEW_OFP_LABEL_DEPARTURE_NAME = "view.ofp.label.departure.name";
  public static final String VIEW_OFP_LABEL_DEPARTURE_TIME = "view.ofp.label.departure.time";
  public static final String VIEW_OFP_LABEL_DEPARTURE_RUNWAY = "view.ofp.label.departure.runway";
  public static final String VIEW_OFP_LABEL_DEPARTURE_PROCEDURE = "view.ofp.label.departure.procedure";
  public static final String VIEW_OFP_LABEL_ARRIVAL = "view.ofp.label.arrival";
  public static final String VIEW_OFP_LABEL_ARRIVAL_ICAO = "view.ofp.label.arrival.icao";
  public static final String VIEW_OFP_LABEL_ARRIVAL_NAME = "view.ofp.label.arrival.name";
  public static final String VIEW_OFP_LABEL_ARRIVAL_TIME = "view.ofp.label.arrival.time";
  public static final String VIEW_OFP_LABEL_ARRIVAL_RUNWAY = "view.ofp.label.arrival.runway";
  public static final String VIEW_OFP_LABEL_ARRIVAL_PROCEDURE = "view.ofp.label.arrival.procedure";
  public static final String VIEW_OFP_LABEL_ROUTE = "view.ofp.label.route";
  public static final String VIEW_OFP_LABEL_TIME_ENROUTE = "view.ofp.label.time.enroute";
  public static final String VIEW_OFP_LABEL_TIME_ENROUTE_VALUE = "view.ofp.label.time.enroute.value";
  public static final String VIEW_OFP_LABEL_AVERAGE_WC = "view.ofp.label.average.wc";

  public static final String VIEW_OFP_TAB_LABEL_ICAO = "view.ofp.tab.label.icao";
  public static final String VIEW_OFP_TAB_LABEL_NAME = "view.ofp.tab.label.name";
  public static final String VIEW_OFP_TAB_LABEL_RUNWAY = "view.ofp.tab.label.runway";
  public static final String VIEW_OFP_TAB_LABEL_METAR = "view.ofp.tab.label.metar";
  public static final String VIEW_OFP_TAB_LABEL_TAF = "view.ofp.tab.label.taf";
  public static final String VIEW_SETTINGS_MENU_FILE = "view.settings.menu.file";
  public static final String VIEW_SETTINGS_MENU_FILE_SAVE = "view.settings.menu.file.save";
  public static final String VIEW_SETTINGS_MENU_FILE_CLOSE = "view.settings.menu.file.close";
  public static final String VIEW_SETTINGS_BUTTON_SAVE = "view.settings.button.save";
  public static final String VIEW_SETTINGS_LABEL_LANGUAGE = "view.settings.label.language";
  public static final String VIEW_SETTINGS_LABEL_SIMBRIEF_USERNAME = "view.settings.label.simbrief.username";
  public static final String VIEW_SETTINGS_LABEL_SIMBRIEF_ID = "view.settings.label.simbrief.id";
  public static final String VIEW_SETTINGS_LABEL_LOG_LEVEL = "view.settings.label.log.level";
  public static final String VIEW_SETTINGS_LABEL_LOG_FILE = "view.settings.label.log.file";
  public static final String VIEW_SELECTION_LABEL_TIME_ZULU = "view.selection.label.time.zulu";
  public static final String VIEW_SELECTION_LABEL_TIME_LOCAL = "view.selection.label.time.local";
  public static final String VIEW_PLANE_DATA = "view.plane.data";
  public static final String VIEW_PLANE_DATA_LABEL_LATITUDE = "view.plane.data.label.latitude";
  public static final String VIEW_PLANE_DATA_LABEL_LONGITUDE = "view.plane.data.label.longitude";
  public static final String VIEW_PLANE_DATA_LABEL_ALTITUDE = "view.plane.data.label.altitude";
  public static final String VIEW_WELCOME_LABEL = "view.welcome.label";
  public static final String I18N_BUNDLE_NAME = "messages";

  /**
   * Used to exclude the unused constructor from code coverage evaluation.
   */
  @Contract(value = " -> fail", pure = true)
  @Generated
  private I18NConstants() {
    throw new IllegalStateException(CommonConstants.UTILITY_CLASS);
  }
}
