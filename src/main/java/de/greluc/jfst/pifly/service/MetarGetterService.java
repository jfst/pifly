////////////////////////////////////////////////////////////////////////////////
// PiFly                                                                       /
// Copyright (C) 2021-2024 PiFly Team                                          /
//                                                                             /
// This file is part of PiFly.                                                 /
//                                                                             /
// PiFly is free software: you can redistribute it and/or modify               /
// it under the terms of the GNU General Public License as published by        /
// the Free Software Foundation, either version 3 of the License, or           /
// (at your option) any later version.                                         /
//                                                                             /
// PiFly is distributed in the hope that it will be useful,                    /
// but WITHOUT ANY WARRANTY; without even the implied warranty of              /
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               /
// GNU General Public License for more details.                                /
//                                                                             /
// You should have received a copy of the GNU General Public License           /
// along with PiFly. If not, see <http://www.gnu.org/licenses/>.               /
////////////////////////////////////////////////////////////////////////////////

package de.greluc.jfst.pifly.service;

import static de.greluc.jfst.pifly.i18n.I18NConstants.NOTIFICATION_METAR_EXCEPTION_IO_CONTENT;
import static de.greluc.jfst.pifly.i18n.I18NConstants.NOTIFICATION_METAR_EXCEPTION_IO_TITLE;
import static de.greluc.jfst.pifly.i18n.I18NConstants.NOTIFICATION_METAR_EXCEPTION_PARSE_CONTENT;
import static de.greluc.jfst.pifly.i18n.I18NConstants.NOTIFICATION_METAR_EXCEPTION_PARSE_TITLE;

import de.greluc.jfst.pifly.event.MetarRequestEvent;
import de.greluc.jfst.pifly.event.MetarResponseEvent;
import de.greluc.jfst.pifly.i18n.I18N;
import io.github.mivek.exception.ParseException;
import io.github.mivek.internationalization.Messages;
import io.github.mivek.service.MetarService;
import java.io.IOException;
import java.net.URISyntaxException;
import javafx.application.Platform;
import javafx.util.Duration;
import lombok.Generated;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.controlsfx.control.Notifications;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;

/**
 * Queries a METAR.
 *
 * @author Lucas Greuloch (greluc, lucas.greuloch@protonmail.com)
 * @version 1.2.1
 * @since 1.0.0
 */
@Service
@Slf4j
public class MetarGetterService {

  private final ApplicationContext applicationContext;
  private final I18N i18N;
  private final MetarService metarService = MetarService.getInstance();

  /**
   * Used for dependency injection.
   *
   * @param applicationContext Spring Boot {@link ApplicationContext} used for event
   *                           publishing.
   * @param i18N               Class that handles the I18N in this project.
   */
  @Contract(pure = true)
  @Autowired
  @Generated
  public MetarGetterService(@NotNull @NonNull ApplicationContext applicationContext, @NotNull @NonNull I18N i18N) {
    this.applicationContext = applicationContext;
    this.i18N = i18N;
    Messages.getInstance().setLocale(i18N.getLocale());
  }

  /**
   * Uses {@link io.github.mivek.parser.MetarParser} to retrieve the live METAR
   * online. Online works with a selected ICAO code.
   *
   * @param event Request event containing the ICAO code for the airport whose METAR should be
   *              retrieved. If no ICAO code is provided the method will return without requesting a METAR.
   */
  @EventListener(MetarRequestEvent.class)
  public void onMetarRequestEvent(@NotNull @NonNull MetarRequestEvent event) {
    if (event.getIcaoCode().isEmpty()) {
      log.info("Requested to retrieve a live METAR online but didn't provide an ICAO code.");
      return;
    }
    try {
      var metar = metarService.retrieveFromAirport(event.getIcaoCode());
      applicationContext.publishEvent(new MetarResponseEvent(this, metar));
    } catch (ParseException exception) {
      log.info("Failed to parse METAR for ICAO code %s".formatted(event.getIcaoCode()));
      if (log.isDebugEnabled()) {
        exception.printStackTrace();
      }
      Platform.runLater(() -> Notifications.create()
          .title(i18N.get(NOTIFICATION_METAR_EXCEPTION_PARSE_TITLE))
          .text(i18N.get(NOTIFICATION_METAR_EXCEPTION_PARSE_CONTENT))
          .hideAfter(Duration.seconds(10))
          .showError());
    } catch (IOException | URISyntaxException exception) {
      log.info("Failed to get METAR for ICAO code %s".formatted(event.getIcaoCode()));
      if (log.isDebugEnabled()) {
        exception.printStackTrace();
      }
      Platform.runLater(() -> Notifications.create()
          .title(i18N.get(NOTIFICATION_METAR_EXCEPTION_IO_TITLE))
          .text(i18N.get(NOTIFICATION_METAR_EXCEPTION_IO_CONTENT))
          .hideAfter(Duration.seconds(10))
          .showError());
    } catch (InterruptedException exception) {
      log.debug("Getting of METAR for ICAO code %s was interrupted".formatted(event.getIcaoCode()));
      if (log.isDebugEnabled()) {
        exception.printStackTrace();
      }
      Thread.currentThread().interrupt();
    }
  }
}