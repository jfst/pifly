////////////////////////////////////////////////////////////////////////////////
// PiFly                                                                       /
// Copyright (C) 2021-2024 PiFly Team                                          /
//                                                                             /
// This file is part of PiFly.                                                 /
//                                                                             /
// PiFly is free software: you can redistribute it and/or modify               /
// it under the terms of the GNU General Public License as published by        /
// the Free Software Foundation, either version 3 of the License, or           /
// (at your option) any later version.                                         /
//                                                                             /
// PiFly is distributed in the hope that it will be useful,                    /
// but WITHOUT ANY WARRANTY; without even the implied warranty of              /
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               /
// GNU General Public License for more details.                                /
//                                                                             /
// You should have received a copy of the GNU General Public License           /
// along with PiFly. If not, see <http://www.gnu.org/licenses/>.               /
////////////////////////////////////////////////////////////////////////////////

package de.greluc.jfst.pifly.service;

import de.greluc.jfst.pifly.data.PreferencesData;
import de.greluc.jfst.pifly.gui.AlertHandler;
import de.greluc.jfst.pifly.i18n.I18N;
import de.greluc.kfst.kfp.data.SourceType;
import jakarta.annotation.PostConstruct;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;
import javafx.scene.control.Alert.AlertType;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Handles the persistence of the preferences.
 *
 * @author Lucas Greuloch (greluc, lucas.greuloch@protonmail.com)
 * @since 1.1.1
 * @version 1.2.1
 */
@Service
@Slf4j
public class PreferencesService {
  private final I18N i18N;
  private final Preferences preferences;
  private final PreferencesData preferencesData;
  private final AlertHandler alertHandler;
  private static final String LANGUAGE = "language";
  private static final String OFP_SIMBRIEF_USERNAME = "ofp.simbrief.username";
  private static final String OFP_SIMBRIEF_ID = "ofp.simbrief.id";
  private static final String OFP_SIMBRIEF_SOURCE = "ofp.simbrief.source";

  @Autowired
  public PreferencesService(I18N i18N, PreferencesData preferencesData, AlertHandler alertHandler) {
    this.i18N = i18N;
    this.alertHandler = alertHandler;
    this.preferencesData = preferencesData;
    preferences = Preferences.userRoot().node(this.getClass().getName());
  }

  @PostConstruct
  private void init() {
    loadPreferences();
  }

  public void persistPreferences() {
    preferences.put(LANGUAGE, preferencesData.getLanguage());
    preferences.put(OFP_SIMBRIEF_USERNAME, preferencesData.getSimbriefUsername());
    preferences.put(OFP_SIMBRIEF_ID, preferencesData.getSimbriefId());
    preferences.putInt(OFP_SIMBRIEF_SOURCE, preferencesData.getSimbriefSource().ordinal());
    try {
      preferences.flush();
    } catch (BackingStoreException exception) {
      log.error("Couldn't persist the preferences!", exception);
      alertHandler.showAlert(AlertType.ERROR, "ERROR", "Error while persisting the preferences!", "Couldn't save the preferences to the persistent storage. Maybe you have no access rights or the storage is full."); // TODO I18N
    }
  }

  public void loadPreferences() {
    try {
      preferences.sync();
    } catch (BackingStoreException e) {
      log.warn("Couldn't load the preferences from the persistent store!");
    }
    preferencesData.setLanguage(preferences.get(LANGUAGE, i18N.getDefaultLocale().toLanguageTag()));
    preferencesData.setSimbriefUsername(preferences.get(OFP_SIMBRIEF_USERNAME, ""));
    preferencesData.setSimbriefId(preferences.get(OFP_SIMBRIEF_ID, ""));
    preferencesData.setSimbriefSource(SourceType.values()[preferences.getInt(OFP_SIMBRIEF_SOURCE, 0)]);
  }
}
