////////////////////////////////////////////////////////////////////////////////
// PiFly                                                                       /
// Copyright (C) 2021-2024 PiFly Team                                          /
//                                                                             /
// This file is part of PiFly.                                                 /
//                                                                             /
// PiFly is free software: you can redistribute it and/or modify               /
// it under the terms of the GNU General Public License as published by        /
// the Free Software Foundation, either version 3 of the License, or           /
// (at your option) any later version.                                         /
//                                                                             /
// PiFly is distributed in the hope that it will be useful,                    /
// but WITHOUT ANY WARRANTY; without even the implied warranty of              /
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               /
// GNU General Public License for more details.                                /
//                                                                             /
// You should have received a copy of the GNU General Public License           /
// along with PiFly. If not, see <http://www.gnu.org/licenses/>.               /
////////////////////////////////////////////////////////////////////////////////

package de.greluc.jfst.pifly.service;

import de.greluc.jfst.jsc.SimConnect;
import de.greluc.jfst.jsc.SimConnectConstants;
import de.greluc.jfst.jsc.SimConnectDataType;
import de.greluc.jfst.jsc.SimConnectPeriod;
import de.greluc.jfst.jsc.recv.DispatcherTask;
import de.greluc.jfst.jsc.recv.OpenHandler;
import de.greluc.jfst.jsc.recv.RecvOpen;
import de.greluc.jfst.jsc.recv.RecvSimObjectData;
import de.greluc.jfst.jsc.recv.SimObjectDataHandler;
import de.greluc.jfst.pifly.event.PositionUpdateEvent;
import jakarta.annotation.PostConstruct;
import java.io.IOException;
import lombok.Generated;
import lombok.Getter;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

/**
 * @author Lucas Greuloch (greluc, lucas.greuloch@protonmail.com)
 * @since 1.0.0
 * @version 1.2.1
 */
@Slf4j
@Component
public class SimConnectPositionService implements OpenHandler, SimObjectDataHandler {

  private final ApplicationContext applicationContext;
  private final Environment env;
  private SimConnect simConnect;
  @Getter
  private double latitude;
  @Getter
  private double longitude;
  @Getter
  private long altitude;

  @Contract(pure = true)
  @Autowired
  @Generated
  public SimConnectPositionService(@NotNull @NonNull ApplicationContext applicationContext,
      @NotNull @NonNull Environment env) {
    this.applicationContext = applicationContext;
    this.env = env;
  }

  @PostConstruct
  @Generated
  private void init() {
    try {
      simConnect = new SimConnect("PiFly-Position", env.getProperty("simconnect.ip", "127.0.0.1"),
          Integer.parseInt(env.getProperty("simconnect.port", "500")));
      buildDataDefinition(simConnect);
      buildDispatcherTask();
      simConnect
          .requestDataOnSimObject(SimConnectDataDefinition.POSITION.ordinal(),
              SimConnectDataDefinition.POSITION.ordinal(),
              SimConnectConstants.OBJECT_ID_USER,
              SimConnectPeriod.SECOND);
    } catch (IOException exception) {
      log.warn("Couldn't access the simulator via SimConnect!");
      log.warn("Maybe wrong SimConnect IP or port in application.properties?");
      log.debug(exception.getMessage(), exception);
    } catch (NumberFormatException exception) {
      log.warn("Couldn't load the SimConnect port from application.properties");
      log.debug(exception.getMessage(), exception);
    }
  }

  private void buildDispatcherTask() {
    DispatcherTask dispatcherTask = new DispatcherTask(simConnect);
    dispatcherTask.addOpenHandler(this);
    dispatcherTask.addSimObjectDataHandler(this);
    var thread = dispatcherTask.createThread();
    thread.setName("Position Dispatcher");
    thread.setDaemon(true);
    thread.start();
  }

  public void handleOpen(@NotNull @NonNull SimConnect sender, @NotNull @NonNull RecvOpen event) {
    log.debug("Connected via SimConnect to: %s %d.%d".formatted(event.getApplicationName(),
        event.getApplicationVersionMajor(), event.getApplicationVersionMinor()));
  }

  public void handleSimObject(@NotNull @NonNull SimConnect sender,
      @NotNull @NonNull RecvSimObjectData event) {
    if (event.getRequestID() == SimConnectDataDefinition.POSITION.ordinal()) {
      latitude = event.getDataFloat64();
      longitude = event.getDataFloat64();
      altitude = Math.round(event.getDataFloat64());
      log.trace("New Position: Lat=%s Lon=%s Alt=%d".formatted(latitude, longitude, altitude));
      applicationContext.publishEvent(new PositionUpdateEvent(this, latitude, longitude, altitude));
    }
  }

  private void buildDataDefinition(@NotNull @NonNull SimConnect simConnect) throws IOException {
    simConnect.addToDataDefinition(SimConnectDataDefinition.POSITION, "PLANE LATITUDE", "DEGREES",
        SimConnectDataType.FLOAT64);
    simConnect.addToDataDefinition(SimConnectDataDefinition.POSITION, "PLANE LONGITUDE", "DEGREES",
        SimConnectDataType.FLOAT64);
    simConnect.addToDataDefinition(SimConnectDataDefinition.POSITION, "PLANE ALTITUDE", "FEET",
        SimConnectDataType.FLOAT64);
  }
}