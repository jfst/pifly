////////////////////////////////////////////////////////////////////////////////
// PiFly                                                                       /
// Copyright (C) 2021-2024 PiFly Team                                          /
//                                                                             /
// This file is part of PiFly.                                                 /
//                                                                             /
// PiFly is free software: you can redistribute it and/or modify               /
// it under the terms of the GNU General Public License as published by        /
// the Free Software Foundation, either version 3 of the License, or           /
// (at your option) any later version.                                         /
//                                                                             /
// PiFly is distributed in the hope that it will be useful,                    /
// but WITHOUT ANY WARRANTY; without even the implied warranty of              /
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               /
// GNU General Public License for more details.                                /
//                                                                             /
// You should have received a copy of the GNU General Public License           /
// along with PiFly. If not, see <http://www.gnu.org/licenses/>.               /
////////////////////////////////////////////////////////////////////////////////

package de.greluc.jfst.pifly.service;

import de.greluc.jfst.pifly.event.MetarRequestEvent;
import de.greluc.jfst.pifly.event.StartWeatherUpdateEvent;
import de.greluc.jfst.pifly.event.StopWeatherUpdateEvent;
import de.greluc.jfst.pifly.event.TafRequestEvent;
import jakarta.annotation.PreDestroy;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;

/**
 * Automatically updates the METAR and TAF data.
 *
 * @author Lucas Greuloch (greluc, lucas.greuloch@protonmail.com)
 * @version 1.2.1
 * @since 1.1.1
 */
@Service
@Slf4j
public class WeatherUpdateService {

  private final ApplicationContext applicationContext;
  private final ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor();
  private Future<?> future;

  /**
   * Used for dependency injection.
   *
   * @param applicationContext Spring Boot {@link ApplicationContext} used for event
   *                           publishing.
   */
  @Autowired
  public WeatherUpdateService(ApplicationContext applicationContext) {
    this.applicationContext = applicationContext;
  }

  /**
   * Starts the update of METAR or TAF then {@link StartWeatherUpdateEvent} is published. Cancels
   * all existing weather updates first.
   *
   * @param event Contains necessary information needed to request METAR or TAF.
   */
  @EventListener(StartWeatherUpdateEvent.class)
  public void onStartWeatherUpdateEvent(@NotNull @NonNull StartWeatherUpdateEvent event) {
    onStopWeatherUpdateEvent();
    if (event.isMetar()) {
      log.debug("Starting continuous METAR update.");
      future = executorService.scheduleAtFixedRate(() -> applicationContext.publishEvent(
              new MetarRequestEvent(this, event.getIcaoCode())), 0, 5,
          TimeUnit.MINUTES);
    } else {
      log.debug("Starting continuous TAF update.");
      future = executorService.scheduleAtFixedRate(() -> applicationContext.publishEvent(
              new TafRequestEvent(this, event.getIcaoCode())), 0, 5,
          TimeUnit.MINUTES);
    }
  }

  /**
   * Stops the update of METAR and TAF then {@link StopWeatherUpdateEvent} is published.
   */
  @EventListener(StopWeatherUpdateEvent.class)
  public void onStopWeatherUpdateEvent() {
    log.debug("Stopping weather updates.");
    if (future != null) {
      future.cancel(true);
    }
  }

  /**
   * Shuts the executors down when the application is exiting.
   */
  @PreDestroy
  public void onExit() {
    log.debug("Stopping weather updates because of application shutdown.");
    if (future != null) {
      future.cancel(true);
    }
    executorService.shutdown();
  }
}
