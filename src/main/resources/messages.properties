################################################################################
# PiFly                                                                        #
# Copyright (C) 2021-2024 PiFly Team                                           #
#                                                                              #
# This file is part of PiFly.                                                  #
#                                                                              #
# PiFly is free software: you can redistribute it and/or modify                #
# it under the terms of the GNU General Public License as published by         #
# the Free Software Foundation, either version 3 of the License, or            #
# (at your option) any later version.                                          #
#                                                                              #
# PiFly is distributed in the hope that it will be useful,                     #
# but WITHOUT ANY WARRANTY; without even the implied warranty of               #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                #
# GNU General Public License for more details.                                 #
#                                                                              #
# You should have received a copy of the GNU General Public License            #
# along with PiFly. If not, see <http://www.gnu.org/licenses/>.                #
################################################################################
view.main.menu.file=Menu
view.main.menu.file.quit=Quit
view.main.menu.help=Help
view.main.menu.help.about=About PiFly
view.main.menu.file.settings=Settings
view.main.menu.ofp=OFP
view.main.menu.ofp.simbrief=simBrief
view.main.menu.ofp.simbrief.local=Local
view.main.menu.ofp.simbrief.online=Online
view.main.title=PiFly
view.settings.button.save=Save
view.settings.label.language=Language:
view.settings.label.simbrief.username=simBrief Username:
view.settings.label.simbrief.id=simBrief ID:
view.settings.label.log.file=Log to file:
view.settings.label.log.level=Log Level:
view.settings.menu.file=Menu
view.settings.menu.file.close=Close
view.settings.menu.file.save=Save
view.selection.label.time.zulu=ZULU:
view.selection.label.time.local=LOCAL:
view.welcome.label=Welcome to PiFly!
alert.error.header=ERROR
alert.error.content.general=An error occurred while performing the desired action.
view.icao.label.airport=Airport ICAO:
view.icao.textfield.prompt=Enter an ICAO code
view.metar=METAR
view.icao=ICAO
view.taf=TAF
view.settings=Settings
view.selection=Selection
view.welcome=Welcome
view.metar.airport.name=Airport:
view.metar.altimeter=Altimiter:
view.metar.wind=Wind:
view.metar.temperature=Temperature:
view.metar.dewpoint=Dew point:
view.metar.area.prompt=Here the METAR will be displayed
view.metar.online=Use online service:
view.metar.cloud.coverage=Cloud coverage:
view.metar.cloud.height=cloud height:
view.metar.cloud.height.unit.feet={0} feet
view.metar.cloud.height.unit.meter={0} meter
view.metar.wind.direction.range={0}\u00B0 ranging from {1}\u00B0 to {2}\u00B0
view.metar.wind.speed.gusts={0}{1} in gusts {2}{1}
view.metar.wind.direction.varying=Varying direction
view.metar.cloud.coverage.nsc=no significant clouds
view.metar.cloud.coverage.skc=sky clear
view.metar.cloud.coverage.few=few clouds (1/8 to 2/8)
view.metar.cloud.coverage.bkn=broken clouds (5/8 to 6/8)
view.metar.cloud.coverage.sct=scattered clouds (3/8 to 4/8)
view.metar.cloud.coverage.ovc=overcast clouds (7/8 to 8/8)
view.metar.wind.direction.fixed={0}\u00B0
view.metar.wind.speed={0}{1}
view.metar.time=Issue time:
view.icao.textfield.tooltip=The ICAO code must consist of four Latin letters
view.metar.visibility=Visibility:
view.icao.error.content=The given ICAO code is not valid!
view.icao.error.title=Error
view.taf.validity=Validity:
view.taf.area.prompt=Here the TAF will be displayed
view.taf.validity.value={0}. {1}:00Z to {2}. {3}:00Z
view.taf.type=Type of trend:
view.taf.validity.value.beginning={0}. {1}:00Z
view.declination.error.title=Attention!
view.declination.error.content=The magnetic declination could not be retrieved! Remember to add it yourself.
notification.taf.exception.parse.title=Error
notification.taf.exception.parse.content=The TAF could not be parsed!
notification.taf.exception.io.title=Error
notification.taf.exception.io.content=The TAF could not be retrieved!
notification.metar.exception.parse.title=Error
notification.metar.exception.parse.content=The METAR could not be parsed!
notification.metar.exception.io.title=Error
notification.metar.exception.io.content=The METAR could not be retrieved!
notification.ofp.exception.file.title=Error
notification.ofp.exception.file.content=The specified file could not be found.
notification.ofp.exception.io.title=Error
notification.ofp.exception.io.content=An error occurred while loading the specified file.
view.ofp.button.load=Load OFP
view.ofp=OFP
view.ofp.button.reset=Reset OFP
view.ofp.error.title=Error
view.ofp.error.content=Couldn't load the OFP!
view.ofp.label.departure=Departure
view.ofp.label.departure.icao=ICAO code:
view.ofp.label.departure.name=Name:
view.ofp.label.departure.time=Takeoff time:
view.ofp.label.departure.runway=Runway:
view.ofp.label.departure.procedure=SID:
view.ofp.label.arrival=Arrival airport
view.ofp.label.arrival.icao=ICAO code:
view.ofp.label.arrival.name=Name:
view.ofp.label.arrival.time=Landing Time:
view.ofp.label.arrival.runway=Runway:
view.ofp.label.arrival.procedure=STAR:
view.ofp.label.route=Route:
view.ofp.label.time.enroute=Time enroute:
view.ofp.label.average.wc=\u00D8 wind:
view.ofp.label.time.enroute.value={0} minutes
view.ofp.tab.label.icao=ICAO:
view.ofp.tab.label.name=NAME:
view.metar.phenomenon=Phenomenon:
view.metar.cloud.coverage.cavok=No Clouds
view.taf.phenomena=Phenomena:
view.ofp.tab.label.runway=Runway:
view.ofp.tab.label.metar=METAR:
view.ofp.tab.label.taf=TAF:
view.plane.data.label.latitude=Latitude:
view.plane.data.label.longitude=Longitude:
view.plane.data.label.altitude=Altitude:
view.plane.data=Plane Data