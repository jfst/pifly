////////////////////////////////////////////////////////////////////////////////
// PiFly                                                                       /
// Copyright (C) 2021-2024 PiFly Team                                          /
//                                                                             /
// This file is part of PiFly.                                                 /
//                                                                             /
// PiFly is free software: you can redistribute it and/or modify               /
// it under the terms of the GNU General Public License as published by        /
// the Free Software Foundation, either version 3 of the License, or           /
// (at your option) any later version.                                         /
//                                                                             /
// PiFly is distributed in the hope that it will be useful,                    /
// but WITHOUT ANY WARRANTY; without even the implied warranty of              /
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               /
// GNU General Public License for more details.                                /
//                                                                             /
// You should have received a copy of the GNU General Public License           /
// along with PiFly. If not, see <http://www.gnu.org/licenses/>.               /
////////////////////////////////////////////////////////////////////////////////

package de.greluc.jfst.pifly.gui;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

/**
 * @author Lucas Greuloch (greluc, lucas.greuloch@protonmail.com)
 * @since 1.0.0
 * @version 1.2.1
 */
class ViewTypeTest {

  @Test
  void testToString() {
    assertEquals("METAR", ViewType.METAR.toString());
    assertEquals("TAF", ViewType.TAF.toString());
    assertEquals("TAFTab", ViewType.TAF_TAB.toString());
    assertEquals("ICAO", ViewType.ICAO.toString());
    assertEquals("Settings", ViewType.SETTINGS.toString());
    assertEquals("Selection", ViewType.SELECTION.toString());
    assertEquals("Welcome", ViewType.WELCOME.toString());
    assertEquals("OFP", ViewType.OFP.toString());
    assertEquals("OFPTab", ViewType.OFP_TAB.toString());
    assertEquals("PlaneData", ViewType.PLANE_DATA.toString());
  }

  @Test
  void values() {
    assertEquals(10, ViewType.values().length);
  }

  @Test
  void valueOf() {
    assertEquals(ViewType.METAR, ViewType.valueOf("METAR"));
    assertEquals(ViewType.TAF, ViewType.valueOf("TAF"));
    assertEquals(ViewType.TAF_TAB, ViewType.valueOf("TAF_TAB"));
    assertEquals(ViewType.ICAO, ViewType.valueOf("ICAO"));
    assertEquals(ViewType.SETTINGS, ViewType.valueOf("SETTINGS"));
    assertEquals(ViewType.SELECTION, ViewType.valueOf("SELECTION"));
    assertEquals(ViewType.WELCOME, ViewType.valueOf("WELCOME"));
    assertEquals(ViewType.OFP, ViewType.valueOf("OFP"));
    assertEquals(ViewType.OFP_TAB, ViewType.valueOf("OFP_TAB"));
    assertEquals(ViewType.PLANE_DATA, ViewType.valueOf("PLANE_DATA"));
  }
}