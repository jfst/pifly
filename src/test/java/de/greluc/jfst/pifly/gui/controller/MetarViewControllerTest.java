////////////////////////////////////////////////////////////////////////////////
// PiFly                                                                       /
// Copyright (C) 2021-2024 PiFly Team                                          /
//                                                                             /
// This file is part of PiFly.                                                 /
//                                                                             /
// PiFly is free software: you can redistribute it and/or modify               /
// it under the terms of the GNU General Public License as published by        /
// the Free Software Foundation, either version 3 of the License, or           /
// (at your option) any later version.                                         /
//                                                                             /
// PiFly is distributed in the hope that it will be useful,                    /
// but WITHOUT ANY WARRANTY; without even the implied warranty of              /
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               /
// GNU General Public License for more details.                                /
//                                                                             /
// You should have received a copy of the GNU General Public License           /
// along with PiFly. If not, see <http://www.gnu.org/licenses/>.               /
////////////////////////////////////////////////////////////////////////////////
//
//package de.greluc.jfst.pifly.gui.controller;
//
//import static org.junit.jupiter.api.Assertions.assertEquals;
//import static org.junit.jupiter.api.Assertions.assertThrows;
//import static org.mockito.ArgumentMatchers.any;
//import static org.mockito.Mockito.doNothing;
//
//import io.github.mivek.exception.ParseException;
//import io.github.mivek.model.Metar;
//import io.github.mivek.service.MetarService;
//import de.greluc.jfst.pifly.data.PreferencesData;
//import de.greluc.jfst.pifly.event.LocaleChangeEvent;
//import de.greluc.jfst.pifly.i18n.I18N;
//import de.greluc.jfst.pifly.i18n.I18NConstants;
//import java.util.Locale;
//import javafx.beans.property.SimpleStringProperty;
//import javafx.beans.property.StringProperty;
//import org.junit.jupiter.api.BeforeAll;
//import org.junit.jupiter.api.BeforeEach;
//import org.junit.jupiter.api.Disabled;
//import org.junit.jupiter.api.Test;
//import org.junit.jupiter.api.extension.ExtendWith;
//import org.mockito.junit.jupiter.MockitoExtension;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.boot.test.mock.mockito.MockBean;
//import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;
//
///**
// * @author Lucas Greuloch (greluc, lucas.greuloch@protonmail.com)
// * @since 1.0.0
// * @version 1.2.1
// */
//@SpringBootTest
//@SpringJUnitConfig
//@ExtendWith(MockitoExtension.class)
//class MetarViewControllerTest {
//
//  private static final String metarString = "EDDF 200920Z 29010G30KT 270V310 9999 FEW022 13/07 Q1018 NOSIG";
//  private static final MetarService service = MetarService.getInstance();
//  private static Metar standardMetar;
//
//  @Autowired
//  private MetarViewController metarViewController;
//  @Autowired
//  private I18N i18N;
//  @Autowired
//  private PreferencesData preferencesData;
//  @MockBean
//  private SelectionViewController selectionViewController;
//
//  @BeforeAll
//  static void initJavaFX() {
//    try {
//      standardMetar = service.decode(metarString);
//    } catch (ParseException ignored) {
//    }
//  }
//
//  @BeforeEach
//  void initMocks() {
//    doNothing().when(selectionViewController).onLocaleChangeEvent(any(LocaleChangeEvent.class));
//  }
//
//  @Test
//  void setAltimeter_InputValid() {
//    StringProperty testProperty = new SimpleStringProperty();
//    metarViewController.setAltimeter(standardMetar, testProperty);
//    assertEquals("1018 hPa", testProperty.get());
//  }
//
//  @SuppressWarnings("ConstantConditions")
//  @Test
//  void setAltimeter_InputNull() {
//    StringProperty testProperty = new SimpleStringProperty();
//    NullPointerException exception = assertThrows(NullPointerException.class,
//        () -> metarViewController.setAltimeter(null, testProperty));
//    assertEquals("metar is marked non-null but is null", exception.getMessage());
//    exception = assertThrows(NullPointerException.class,
//        () -> metarViewController.setAltimeter(standardMetar, null));
//    assertEquals("stringProperty is marked non-null but is null", exception.getMessage());
//  }
//
//  @Test
//  void setTemperature_InputValid() {
//    StringProperty testProperty = new SimpleStringProperty();
//    metarViewController.setTemperature(standardMetar, testProperty, false);
//    assertEquals("13°C", testProperty.get());
//    metarViewController.setTemperature(standardMetar, testProperty, true);
//    assertEquals("7°C", testProperty.get());
//  }
//
//  @SuppressWarnings("ConstantConditions")
//  @Test
//  void setTemperature_InputNull() {
//    StringProperty testProperty = new SimpleStringProperty();
//    NullPointerException exception = assertThrows(NullPointerException.class,
//        () -> metarViewController.setTemperature(null, testProperty, true));
//    assertEquals("metar is marked non-null but is null", exception.getMessage());
//    exception = assertThrows(NullPointerException.class,
//        () -> metarViewController.setTemperature(standardMetar, null, true));
//    assertEquals("stringProperty is marked non-null but is null", exception.getMessage());
//  }
//
//  @Test
//  @Disabled
//  void setCloudsHeight_InputValid_LocaleEnglish() {
//    preferencesData.setLanguage(Locale.ENGLISH.toLanguageTag());
//    i18N.setLocale();
//    StringProperty testProperty = new SimpleStringProperty();
//    metarViewController.setCloudsHeight(standardMetar, testProperty);
//    assertEquals("2,200 feet", testProperty.get());
//  }
//
//  @Test
//  @Disabled
//  void setCloudsHeight_InputValid_LocaleGerman() {
//    preferencesData.setLanguage(Locale.GERMAN.toLanguageTag());
//    i18N.setLocale();
//    StringProperty testProperty = new SimpleStringProperty();
//    metarViewController.setCloudsHeight(standardMetar, testProperty);
//    assertEquals("2.200 Fuß", testProperty.get());
//  }
//
//  @Test
//  @Disabled
//  void setCloudsHeight_InputValid_NoClouds_LocaleEnglish() {
//    try {
//      StringProperty testProperty = new SimpleStringProperty();
//      Metar testMetar = service
//          .decode("EDDF 200920Z 29010G30KT 270V310 CAVOK 13/07 Q1018 NOSIG");
//      preferencesData.setLanguage(Locale.ENGLISH.toLanguageTag());
//      i18N.setLocale();
//      metarViewController.setCloudsHeight(testMetar, testProperty);
//      assertEquals("", testProperty.get());
//    } catch (ParseException ignored) {
//
//    }
//  }
//
//  @Test
//  @Disabled
//  void setCloudsHeight_InputValid_NoClouds_LocaleGerman() {
//    try {
//      StringProperty testProperty = new SimpleStringProperty();
//      Metar testMetar = service
//          .decode("EDDF 200920Z 29010G30KT 270V310 CAVOK 13/07 Q1018 NOSIG");
//      preferencesData.setLanguage(Locale.GERMAN.toLanguageTag());
//      i18N.setLocale();
//      metarViewController.setCloudsHeight(testMetar, testProperty);
//      assertEquals("", testProperty.get());
//    } catch (ParseException ignored) {
//
//    }
//  }
//
//  @SuppressWarnings("ConstantConditions")
//  @Test
//  void setCloudsHeight_InputNull() {
//    StringProperty testProperty = new SimpleStringProperty();
//    NullPointerException exception = assertThrows(NullPointerException.class,
//        () -> metarViewController.setCloudsHeight(null, testProperty));
//    assertEquals("metar is marked non-null but is null", exception.getMessage());
//    exception = assertThrows(NullPointerException.class,
//        () -> metarViewController.setCloudsHeight(standardMetar, null));
//    assertEquals("stringProperty is marked non-null but is null", exception.getMessage());
//  }
//
//  @Test
//  @Disabled
//  void setCloudCoverage_InputValid_FEW_LocaleEnglish() {
//    try {
//      StringProperty testProperty = new SimpleStringProperty();
//      Metar testMetar = service
//          .decode("EDDF 200920Z 29010G30KT 270V310 9999 FEW022 13/07 Q1018 NOSIG");
//      preferencesData.setLanguage(Locale.ENGLISH.toLanguageTag());
//      i18N.setLocale();
//      metarViewController.setCloudCoverage(testMetar, testProperty);
//      assertEquals(i18N.get(I18NConstants.VIEW_METAR_CLOUD_COVERAGE_FEW), testProperty.get());
//    } catch (ParseException ignored) {
//
//    }
//  }
//
//  @Test
//  @Disabled
//  void setCloudCoverage_InputValid_FEW_LocaleGerman() {
//    try {
//      StringProperty testProperty = new SimpleStringProperty();
//      Metar testMetar = service
//          .decode("EDDF 200920Z 29010G30KT 270V310 9999 FEW022 13/07 Q1018 NOSIG");
//      preferencesData.setLanguage(Locale.GERMAN.toLanguageTag());
//      i18N.setLocale();
//      metarViewController.setCloudCoverage(testMetar, testProperty);
//      assertEquals(i18N.get(I18NConstants.VIEW_METAR_CLOUD_COVERAGE_FEW), testProperty.get());
//    } catch (ParseException ignored) {
//
//    }
//  }
//
//  @Test
//  @Disabled
//  void setCloudCoverage_InputValid_SCT_LocaleEnglish() {
//    try {
//      StringProperty testProperty = new SimpleStringProperty();
//      Metar testMetar = service
//          .decode("EDDF 200920Z 29010G30KT 270V310 9999 SCT022 13/07 Q1018 NOSIG");
//      preferencesData.setLanguage(Locale.ENGLISH.toLanguageTag());
//      i18N.setLocale();
//      metarViewController.setCloudCoverage(testMetar, testProperty);
//      assertEquals(i18N.get(I18NConstants.VIEW_METAR_CLOUD_COVERAGE_SCT), testProperty.get());
//    } catch (ParseException ignored) {
//
//    }
//  }
//
//  @Test
//  @Disabled
//  void setCloudCoverage_InputValid_SCT_LocaleGerman() {
//    try {
//      StringProperty testProperty = new SimpleStringProperty();
//      Metar testMetar = service
//          .decode("EDDF 200920Z 29010G30KT 270V310 9999 SCT022 13/07 Q1018 NOSIG");
//      preferencesData.setLanguage(Locale.GERMAN.toLanguageTag());
//      i18N.setLocale();
//      metarViewController.setCloudCoverage(testMetar, testProperty);
//      assertEquals(i18N.get(I18NConstants.VIEW_METAR_CLOUD_COVERAGE_SCT), testProperty.get());
//    } catch (ParseException ignored) {
//
//    }
//  }
//
//  @Test
//  @Disabled
//  void setCloudCoverage_InputValid_BKN_LocaleEnglish() {
//    try {
//      StringProperty testProperty = new SimpleStringProperty();
//      Metar testMetar = service
//          .decode("EDDF 200920Z 29010G30KT 270V310 9999 BKN022 13/07 Q1018 NOSIG");
//      preferencesData.setLanguage(Locale.ENGLISH.toLanguageTag());
//      i18N.setLocale();
//      metarViewController.setCloudCoverage(testMetar, testProperty);
//      assertEquals(i18N.get(I18NConstants.VIEW_METAR_CLOUD_COVERAGE_BKN), testProperty.get());
//    } catch (ParseException ignored) {
//
//    }
//  }
//
//  @Test
//  @Disabled
//  void setCloudCoverage_InputValid_BKN_LocaleGerman() {
//    try {
//      StringProperty testProperty = new SimpleStringProperty();
//      Metar testMetar = service
//          .decode("EDDF 200920Z 29010G30KT 270V310 9999 BKN022 13/07 Q1018 NOSIG");
//      preferencesData.setLanguage(Locale.GERMAN.toLanguageTag());
//      i18N.setLocale();
//      metarViewController.setCloudCoverage(testMetar, testProperty);
//      assertEquals(i18N.get(I18NConstants.VIEW_METAR_CLOUD_COVERAGE_BKN), testProperty.get());
//    } catch (ParseException ignored) {
//
//    }
//  }
//
//  @Test
//  @Disabled
//  void setCloudCoverage_InputValid_OVC_LocaleEnglish() {
//    try {
//      StringProperty testProperty = new SimpleStringProperty();
//      Metar testMetar = service
//          .decode("EDDF 200920Z 29010G30KT 270V310 9999 OVC022 13/07 Q1018 NOSIG");
//      preferencesData.setLanguage(Locale.ENGLISH.toLanguageTag());
//      i18N.setLocale();
//      metarViewController.setCloudCoverage(testMetar, testProperty);
//      assertEquals(i18N.get(I18NConstants.VIEW_METAR_CLOUD_COVERAGE_OVC), testProperty.get());
//    } catch (ParseException ignored) {
//
//    }
//  }
//
//  @Test
//  @Disabled
//  void setCloudCoverage_InputValid_OVC_LocaleGerman() {
//    try {
//      StringProperty testProperty = new SimpleStringProperty();
//      Metar testMetar = service
//          .decode("EDDF 200920Z 29010G30KT 270V310 9999 OVC022 13/07 Q1018 NOSIG");
//      preferencesData.setLanguage(Locale.GERMAN.toLanguageTag());
//      i18N.setLocale();
//      metarViewController.setCloudCoverage(testMetar, testProperty);
//      assertEquals(i18N.get(I18NConstants.VIEW_METAR_CLOUD_COVERAGE_OVC), testProperty.get());
//    } catch (ParseException ignored) {
//
//    }
//  }
//
//  @Test
//  @Disabled
//  void setCloudCoverage_InputValid_NSC_LocaleEnglish() {
//    try {
//      StringProperty testProperty = new SimpleStringProperty();
//      Metar testMetar = service
//          .decode("EDDF 200920Z 29010G30KT 270V310 9999 NSC 13/07 Q1018 NOSIG");
//      preferencesData.setLanguage(Locale.ENGLISH.toLanguageTag());
//      i18N.setLocale();
//      metarViewController.setCloudCoverage(testMetar, testProperty);
//      assertEquals(i18N.get(I18NConstants.VIEW_METAR_CLOUD_COVERAGE_NSC), testProperty.get());
//    } catch (ParseException ignored) {
//
//    }
//  }
//
//  @Test
//  @Disabled
//  void setCloudCoverage_InputValid_NSC_LocaleGerman() {
//    try {
//      StringProperty testProperty = new SimpleStringProperty();
//      Metar testMetar = service
//          .decode("EDDF 200920Z 29010G30KT 270V310 9999 NSC 13/07 Q1018 NOSIG");
//      preferencesData.setLanguage(Locale.GERMAN.toLanguageTag());
//      i18N.setLocale();
//      metarViewController.setCloudCoverage(testMetar, testProperty);
//      assertEquals(i18N.get(I18NConstants.VIEW_METAR_CLOUD_COVERAGE_NSC), testProperty.get());
//    } catch (ParseException ignored) {
//
//    }
//  }
//
//  @Test
//  @Disabled
//  void setCloudCoverage_InputValid_SKC_LocaleEnglish() {
//    try {
//      StringProperty testProperty = new SimpleStringProperty();
//      Metar testMetar = service
//          .decode("EDDF 200920Z 29010G30KT 270V310 9999 SKC 13/07 Q1018 NOSIG");
//      preferencesData.setLanguage(Locale.ENGLISH.toLanguageTag());
//      i18N.setLocale();
//      metarViewController.setCloudCoverage(testMetar, testProperty);
//      assertEquals(i18N.get(I18NConstants.VIEW_METAR_CLOUD_COVERAGE_SKC), testProperty.get());
//    } catch (ParseException ignored) {
//
//    }
//  }
//
//  @Test
//  @Disabled
//  void setCloudCoverage_InputValid_SKC_LocaleGerman() {
//    try {
//      StringProperty testProperty = new SimpleStringProperty();
//      Metar testMetar = service
//          .decode("EDDF 200920Z 29010G30KT 270V310 9999 SKC 13/07 Q1018 NOSIG");
//      preferencesData.setLanguage(Locale.GERMAN.toLanguageTag());
//      i18N.setLocale();
//      metarViewController.setCloudCoverage(testMetar, testProperty);
//      assertEquals(i18N.get(I18NConstants.VIEW_METAR_CLOUD_COVERAGE_SKC), testProperty.get());
//    } catch (ParseException ignored) {
//
//    }
//  }
//
//  @Test
//  @Disabled
//  void setCloudCoverage_InputValid_NoClouds_LocaleEnglish() {
//    try {
//      StringProperty testProperty = new SimpleStringProperty();
//      Metar testMetar = service
//          .decode("EDDF 200920Z 29010G30KT 270V310 CAVOK 13/07 Q1018 NOSIG");
//      preferencesData.setLanguage(Locale.ENGLISH.toLanguageTag());
//      i18N.setLocale();
//      metarViewController.setCloudCoverage(testMetar, testProperty);
//      assertEquals("No Clouds", testProperty.get());
//    } catch (ParseException ignored) {
//
//    }
//  }
//
//  @Test
//  @Disabled
//  void setCloudCoverage_InputValid_NoClouds_LocaleGerman() {
//    try {
//      StringProperty testProperty = new SimpleStringProperty();
//      Metar testMetar = service
//          .decode("EDDF 200920Z 29010G30KT 270V310 CAVOK 13/07 Q1018 NOSIG");
//      preferencesData.setLanguage(Locale.GERMAN.toLanguageTag());
//      i18N.setLocale();
//      metarViewController.setCloudCoverage(testMetar, testProperty);
//      assertEquals("Keine Bewölkung", testProperty.get());
//    } catch (ParseException ignored) {
//
//    }
//  }
//
//  @SuppressWarnings("ConstantConditions")
//  @Test
//  void setCloudCoverage_InputNull() {
//    StringProperty testProperty = new SimpleStringProperty();
//    NullPointerException exception = assertThrows(NullPointerException.class,
//        () -> metarViewController.setCloudCoverage(null, testProperty));
//    assertEquals("metar is marked non-null but is null", exception.getMessage());
//    exception = assertThrows(NullPointerException.class,
//        () -> metarViewController.setCloudCoverage(standardMetar, null));
//    assertEquals("stringProperty is marked non-null but is null", exception.getMessage());
//  }
//
//  @Test
//  void setIssueTime_InputValid() {
//    StringProperty testProperty = new SimpleStringProperty();
//    metarViewController.setIssueTime(standardMetar, testProperty);
//    assertEquals("09:20Z", testProperty.get());
//  }
//
//  @SuppressWarnings("ConstantConditions")
//  @Test
//  void setIssueTime_InputNull() {
//    StringProperty testProperty = new SimpleStringProperty();
//    NullPointerException exception = assertThrows(NullPointerException.class,
//        () -> metarViewController.setIssueTime(null, testProperty));
//    assertEquals("metar is marked non-null but is null", exception.getMessage());
//    exception = assertThrows(NullPointerException.class,
//        () -> metarViewController.setIssueTime(standardMetar, null));
//    assertEquals("stringProperty is marked non-null but is null", exception.getMessage());
//  }
//
//  @Test
//  @Disabled
//  void setWindDirection_InputValid_NoVaryingNoRange_LocaleEnglish() {
//    try {
//      Metar testMetar = service
//          .decode("EDDF 200920Z 29010G30KT CAVOK 13/07 Q1018 NOSIG");
//      preferencesData.setLanguage(Locale.ENGLISH.toLanguageTag());
//      i18N.setLocale();
//      assertEquals(i18N.get(I18NConstants.VIEW_METAR_WIND_DIRECTION_FIXED, 290),
//          metarViewController.setWindDirection(testMetar));
//    } catch (ParseException ignored) {
//
//    }
//  }
//
//  @Test
//  @Disabled
//  void setWindDirection_InputValid_NoVaryingNoRange_LocaleGerman() {
//    try {
//      Metar testMetar = service
//          .decode("EDDF 200920Z 29010G30KT CAVOK 13/07 Q1018 NOSIG");
//      preferencesData.setLanguage(Locale.GERMAN.toLanguageTag());
//      i18N.setLocale();
//      assertEquals(i18N.get(I18NConstants.VIEW_METAR_WIND_DIRECTION_FIXED, 290),
//          metarViewController.setWindDirection(testMetar));
//    } catch (ParseException ignored) {
//
//    }
//  }
//
//  @Test
//  @Disabled
//  void setWindDirection_InputValid_VaryingNoRange_LocaleEnglish() {
//    try {
//      Metar testMetar = service
//          .decode("EDDF 200920Z VRB02KT CAVOK 13/07 Q1018 NOSIG");
//      preferencesData.setLanguage(Locale.ENGLISH.toLanguageTag());
//      i18N.setLocale();
//      assertEquals(i18N.get(I18NConstants.VIEW_METAR_WIND_DIRECTION_VARYING),
//          metarViewController.setWindDirection(testMetar));
//    } catch (ParseException ignored) {
//
//    }
//  }
//
//  @Test
//  @Disabled
//  void setWindDirection_InputValid_VaryingNoRange_LocaleGerman() {
//    try {
//      Metar testMetar = service
//          .decode("EDDF 200920Z VRB02KT CAVOK 13/07 Q1018 NOSIG");
//      preferencesData.setLanguage(Locale.GERMAN.toLanguageTag());
//      i18N.setLocale();
//      assertEquals(i18N.get(I18NConstants.VIEW_METAR_WIND_DIRECTION_VARYING),
//          metarViewController.setWindDirection(testMetar));
//    } catch (ParseException ignored) {
//
//    }
//  }
//
//  @Test
//  @Disabled
//  void setWindDirection_InputValid_NoVaryingRange_LocaleEnglish() {
//    preferencesData.setLanguage(Locale.ENGLISH.toLanguageTag());
//    i18N.setLocale();
//    assertEquals(i18N.get(I18NConstants.VIEW_METAR_WIND_DIRECTION_RANGE, 290, 270, 310),
//        metarViewController.setWindDirection(standardMetar));
//  }
//
//  @Test
//  @Disabled
//  void setWindDirection_InputValid_NoVaryingRange_LocaleGerman() {
//    preferencesData.setLanguage(Locale.GERMAN.toLanguageTag());
//    i18N.setLocale();
//    assertEquals(i18N.get(I18NConstants.VIEW_METAR_WIND_DIRECTION_RANGE, 290, 270, 310),
//        metarViewController.setWindDirection(standardMetar));
//  }
//
//  @SuppressWarnings("ConstantConditions")
//  @Test
//  void setWindDirection_InputNull() {
//    NullPointerException exception = assertThrows(NullPointerException.class,
//        () -> metarViewController.setWindDirection(null));
//    assertEquals("metar is marked non-null but is null", exception.getMessage());
//  }
//
//  @Test
//  @Disabled
//  void setWindSpeed_InputValid_NoGusts_LocaleEnglish() {
//    try {
//      Metar testMetar = service.decode("EDDF 200920Z 29010KT 270V310 CAVOK 13/07 Q1018 NOSIG");
//      preferencesData.setLanguage(Locale.ENGLISH.toLanguageTag());
//      i18N.setLocale();
//      assertEquals(i18N.get(I18NConstants.VIEW_METAR_WIND_SPEED, 10, "KT"),
//          metarViewController.setWindSpeed(testMetar));
//    } catch (ParseException ignored) {
//
//    }
//  }
//
//  @Test
//  @Disabled
//  void setWindSpeed_InputValid_NoGusts_LocaleGerman() {
//    try {
//      Metar testMetar = service.decode("EDDF 200920Z 29010KT 270V310 CAVOK 13/07 Q1018 NOSIG");
//      preferencesData.setLanguage(Locale.GERMAN.toLanguageTag());
//      i18N.setLocale();
//      assertEquals(i18N.get(I18NConstants.VIEW_METAR_WIND_SPEED, 10, "KT"),
//          metarViewController.setWindSpeed(testMetar));
//    } catch (ParseException ignored) {
//
//    }
//  }
//
//  @Test
//  @Disabled
//  void setWindSpeed_InputValid_Gusts_LocaleEnglish() {
//    preferencesData.setLanguage(Locale.ENGLISH.toLanguageTag());
//    i18N.setLocale();
//    assertEquals(i18N.get(I18NConstants.VIEW_METAR_WIND_SPEED_GUSTS, 10, "KT", 30),
//        metarViewController.setWindSpeed(standardMetar));
//  }
//
//  @Test
//  @Disabled
//  void setWindSpeed_InputValid_Gusts_LocaleGerman() {
//    preferencesData.setLanguage(Locale.GERMAN.toLanguageTag());
//    i18N.setLocale();
//    assertEquals(i18N.get(I18NConstants.VIEW_METAR_WIND_SPEED_GUSTS, 10, "KT", 30),
//        metarViewController.setWindSpeed(standardMetar));
//  }
//
//  @SuppressWarnings("ConstantConditions")
//  @Test
//  void setWindSpeed_InputNull() {
//    NullPointerException exception = assertThrows(NullPointerException.class,
//        () -> metarViewController.setWindSpeed(null));
//    assertEquals("metar is marked non-null but is null", exception.getMessage());
//  }
//}