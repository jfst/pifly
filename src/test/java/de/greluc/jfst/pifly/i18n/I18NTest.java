////////////////////////////////////////////////////////////////////////////////
// PiFly                                                                       /
// Copyright (C) 2021-2024 PiFly Team                                          /
//                                                                             /
// This file is part of PiFly.                                                 /
//                                                                             /
// PiFly is free software: you can redistribute it and/or modify               /
// it under the terms of the GNU General Public License as published by        /
// the Free Software Foundation, either version 3 of the License, or           /
// (at your option) any later version.                                         /
//                                                                             /
// PiFly is distributed in the hope that it will be useful,                    /
// but WITHOUT ANY WARRANTY; without even the implied warranty of              /
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               /
// GNU General Public License for more details.                                /
//                                                                             /
// You should have received a copy of the GNU General Public License           /
// along with PiFly. If not, see <http://www.gnu.org/licenses/>.               /
////////////////////////////////////////////////////////////////////////////////
//
//package de.greluc.jfst.pifly.i18n;
//
//import de.greluc.jfst.pifly.data.PreferencesData;
//import de.greluc.jfst.pifly.gui.controller.SelectionViewController;
//import javafx.beans.property.SimpleStringProperty;
//import javafx.beans.property.StringProperty;
//import javafx.collections.ObservableList;
//import org.junit.jupiter.api.Test;
//import org.junit.jupiter.api.extension.ExtendWith;
//import org.mockito.junit.jupiter.MockitoExtension;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.boot.test.mock.mockito.MockBean;
//import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;
//
//import java.util.Locale;
//import java.util.ResourceBundle;
//
//import static org.junit.jupiter.api.Assertions.*;
//import static org.mockito.Mockito.doNothing;
//
///**
// * @author Lucas Greuloch (greluc, lucas.greuloch@protonmail.com)
// * @since 1.0.0
// * @version 1.2.1
// */
//@SpringBootTest
//@SpringJUnitConfig
//@ExtendWith(MockitoExtension.class)
//class I18NTest {
//
//  @Autowired
//  private I18N i18N;
//  @Autowired
//  private PreferencesData preferencesData;
//
//  @MockBean
//  private SelectionViewController selectionViewController;
//
//  @SuppressWarnings("ConstantConditions")
//  @Test
//  void constructor_InputNull() {
//    NullPointerException exception = assertThrows(NullPointerException.class, () -> new I18N(null,
//        preferencesData));
//    assertEquals("applicationContext is marked non-null but is null", exception.getMessage());
//  }
//
//  @Test
//  void getSupportedLocales() {
//    ObservableList<Locale> locales = i18N.getSupportedLocales();
//    assertEquals(4, locales.size());
//    assertTrue(locales.contains(Locale.ENGLISH));
//    assertTrue(locales.contains(Locale.US));
//    assertTrue(locales.contains(Locale.GERMAN));
//    assertTrue(locales.contains(Locale.GERMANY));
//  }
//
//  @Test
//  void getDefaultLocale_DefaultLocaleSupported() {
//    doNothing().when(selectionViewController).onLocaleChangeEvent();
//    Locale.setDefault(Locale.ENGLISH);
//    assertEquals(Locale.ENGLISH, i18N.getDefaultLocale());
//    Locale.setDefault(Locale.US);
//    assertEquals(Locale.US, i18N.getDefaultLocale());
//    Locale.setDefault(Locale.GERMAN);
//    assertEquals(Locale.GERMAN, i18N.getDefaultLocale());
//    Locale.setDefault(Locale.GERMANY);
//    assertEquals(Locale.GERMANY, i18N.getDefaultLocale());
//  }
//
//  @Test
//  void getDefaultLocale_DefaultLocaleNotSupported() {
//    Locale.setDefault(Locale.SIMPLIFIED_CHINESE);
//    assertEquals(Locale.ENGLISH, i18N.getDefaultLocale());
//    Locale.setDefault(Locale.CANADA_FRENCH);
//    assertEquals(Locale.ENGLISH, i18N.getDefaultLocale());
//  }
//
//  @Test
//  void getLocale() {
//    preferencesData.setLanguage(Locale.ENGLISH.toLanguageTag());
//    i18N.setLocale();
//    assertEquals(Locale.ENGLISH, i18N.getLocale());
//    preferencesData.setLanguage(Locale.US.toLanguageTag());
//    i18N.setLocale();
//    assertEquals(Locale.US, i18N.getLocale());
//    preferencesData.setLanguage(Locale.GERMAN.toLanguageTag());
//    i18N.setLocale();
//    assertEquals(Locale.GERMAN, i18N.getLocale());
//    preferencesData.setLanguage(Locale.GERMANY.toLanguageTag());
//    i18N.setLocale();
//    assertEquals(Locale.GERMANY, i18N.getLocale());
//  }
//
//  @Test
//  void setLocale_InputSupported() {
//    preferencesData.setLanguage(Locale.ENGLISH.toLanguageTag());
//    i18N.setLocale();
//    assertEquals(Locale.ENGLISH, i18N.getLocale());
//    assertEquals(Locale.ENGLISH, Locale.getDefault());
//    preferencesData.setLanguage(Locale.US.toLanguageTag());
//    i18N.setLocale();
//    assertEquals(Locale.US, i18N.getLocale());
//    assertEquals(Locale.US, Locale.getDefault());
//    preferencesData.setLanguage(Locale.GERMAN.toLanguageTag());
//    i18N.setLocale();
//    assertEquals(Locale.GERMAN, i18N.getLocale());
//    assertEquals(Locale.GERMAN, Locale.getDefault());
//    preferencesData.setLanguage(Locale.GERMANY.toLanguageTag());
//    i18N.setLocale();
//    assertEquals(Locale.GERMANY, i18N.getLocale());
//    assertEquals(Locale.GERMANY, Locale.getDefault());
//  }
//
//  @Test
//  void setLocale_InputNotSupported() {
//    preferencesData.setLanguage(Locale.SIMPLIFIED_CHINESE.toLanguageTag());
//    i18N.setLocale();
//    assertEquals(Locale.ENGLISH, i18N.getLocale());
//    assertEquals(Locale.ENGLISH, Locale.getDefault());
//    preferencesData.setLanguage(Locale.CANADA_FRENCH.toLanguageTag());
//    i18N.setLocale();
//    assertEquals(Locale.ENGLISH, i18N.getLocale());
//    assertEquals(Locale.ENGLISH, Locale.getDefault());
//  }
//
//  @Test
//  void get_InputValid() {
//    ResourceBundle bundle = ResourceBundle
//        .getBundle(I18NConstants.I18N_BUNDLE_NAME, Locale.ENGLISH);
//    preferencesData.setLanguage(Locale.ENGLISH.toLanguageTag());
//    i18N.setLocale();
//    assertEquals(bundle.getString("view.main.menu.file"), i18N.get("view.main.menu.file"));
//    bundle = ResourceBundle.getBundle(I18NConstants.I18N_BUNDLE_NAME, Locale.US);
//    preferencesData.setLanguage(Locale.US.toLanguageTag());
//    i18N.setLocale();
//    assertEquals(bundle.getString("view.main.menu.file"), i18N.get("view.main.menu.file"));
//    bundle = ResourceBundle.getBundle(I18NConstants.I18N_BUNDLE_NAME, Locale.GERMAN);
//    preferencesData.setLanguage(Locale.GERMAN.toLanguageTag());
//    i18N.setLocale();
//    assertEquals(bundle.getString("view.main.menu.file"), i18N.get("view.main.menu.file"));
//    bundle = ResourceBundle.getBundle(I18NConstants.I18N_BUNDLE_NAME, Locale.GERMANY);
//    preferencesData.setLanguage(Locale.GERMANY.toLanguageTag());
//    i18N.setLocale();
//    assertEquals(bundle.getString("view.main.menu.file"), i18N.get("view.main.menu.file"));
//  }
//
//  @SuppressWarnings("ConstantConditions")
//  @Test
//  void get_InputNull() {
//    NullPointerException exception = assertThrows(NullPointerException.class, () -> i18N.get(null));
//    assertEquals("key is marked non-null but is null", exception.getMessage());
//  }
//
//  @Test
//  void initBinding_InputValid() {
//    ResourceBundle bundle = ResourceBundle
//        .getBundle(I18NConstants.I18N_BUNDLE_NAME, Locale.ENGLISH);
//    preferencesData.setLanguage(Locale.ENGLISH.toLanguageTag());
//    i18N.setLocale();
//    SimpleStringProperty aProperty = new SimpleStringProperty();
//    i18N.initBinding(aProperty, "view.main.menu.file");
//    assertEquals(bundle.getString("view.main.menu.file"), aProperty.get());
//    SimpleStringProperty bProperty = new SimpleStringProperty();
//    i18N.initBinding(bProperty, "view.main.menu.help");
//    assertEquals(bundle.getString("view.main.menu.help"), bProperty.get());
//  }
//
//  @SuppressWarnings("ConstantConditions")
//  @Test
//  void initBinding_InputNull() {
//    NullPointerException exception = assertThrows(NullPointerException.class,
//        () -> i18N.initBinding(null, ""));
//    assertEquals("stringProperty is marked non-null but is null", exception.getMessage());
//    StringProperty stringProperty = new SimpleStringProperty();
//    exception = assertThrows(NullPointerException.class,
//        () -> i18N.initBinding(stringProperty, null));
//    assertEquals("key is marked non-null but is null", exception.getMessage());
//  }
//}